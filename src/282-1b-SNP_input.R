# Title     : SNP input
# Objective : Merge VCF files into GDS inputs
# Created by: Guillaume Ramstein (gr226@cornell.edu)
# Created on: 11/29/2018

#--------------------------------------------------------
# Script parameters
#--------------------------------------------------------
# Overwrite GDS file
overwrite <- FALSE

# Working directory
wd <- "~/QGG_plants/snp_constraint_prediction"
dir.create(wd, showWarnings = FALSE)
setwd(wd)

# Input data
input_dir <- "data/282set/vcf"
gds_file <- "data/282set/AGPv3_Hmp321.gds"
GFF_file <- "data/B73/Zea_mays.AGPv3.31.gtf"

# GRM parameters
chromosomes <- 1:10
min_maf <- 0.01
n_PCs <- 3

n_cores <- 20

cis_range <- 1e5
trans_range <- 1e6

# Output data
map_file <- "data/282set/SNP_map.rds"
G_file <- "data/282set/G.rds"
ED_file <- "data/282set/ED.rds"
Q_file <- "data/282set/Q.rds"

X_file <- "data/282set/AGPv3_282set.rds"
X.GDS_file <- "data/282set/AGPv3_282set.gds"
X.BED_prefix <- "data/282set/AGPv3_282set"

XXt_files <- c("genome-wide"="data/282set/GRMs/XXt.rds",
               "cis"="data/282set/GRMs/XXt_cis.rds",
               "trans"="data/282set/GRMs/XXt_trans.rds")

XXt_folders <- c("cis"="data/282set/GRMs/cis/",
                 "trans"="data/282set/GRMs/trans/")
for (folder in XXt_folders) dir.create(folder, showWarnings=FALSE)

cis_SNPs_folder <- "data/282set/cis_SNPs/"
dir.create(cis_SNPs_folder, showWarnings=FALSE)

#--------------------------------------------------------
# Functions
#--------------------------------------------------------
# Functions
library(foreach)
library(data.table)
if (! "SNPRelate" %in% rownames(installed.packages())) {
  source("http://bioconductor.org/biocLite.R")
  biocLite("SNPRelate")
}
library(SNPRelate)
library(dplyr)
library(GenomicFeatures)

#########################################################
# Genotypic data
#########################################################
# Input VCF files
vcf_files <- list.files(input_dir, full.names=TRUE, pattern="vcf.gz$")

# Making GDS file
if (!file.exists(gds_file) || overwrite) {
  
  snpgdsVCF2GDS(vcf_files, gds_file)
  
}

# Genotypic data
geno_Hmp <- snpgdsOpen(gds_file)

# Selected samples
sample_info <- snpgdsSummary(geno_Hmp)
selected_samples <- sample_info$sample.id[grep("282set", sample_info$sample.id)]

# Reference counts
X_list <- snpgdsGetGeno(geno_Hmp,
                        sample.id=selected_samples,
                        snpfirstdim=FALSE,
                        with.id=TRUE)

stopifnot(all(selected_samples == X_list$sample.id))

X <- X_list$genotype
rownames(X) <- gsub("282set_", "", gsub("Goodman-Buckler", "", X_list$sample.id))
colnames(X) <- X_list$snp.id

rm(X_list)
gc()

# SNP map
map <- data.table(snpgdsSNPList(geno_Hmp))
setkey(map, snp.id)

map$chromosome <- as.integer(map$chromosome)
map$snp <- paste0("S", map$chromosome, "_", map$position)

af <- colMeans(X)/2
map$maf <- pmin(af, 1-af)

saveRDS(map, map_file)

# Selected SNPs
selected_SNPs <- map$snp.id[map$maf >= min_maf & map$chromosome %in% chromosomes]

snp_pos <- map[selected_SNPs]$position %>% IRanges(., .)
snp_rs <-  map[selected_SNPs]$snp

X <- X[, selected_SNPs]
colnames(X) <- snp_rs

# Genotype data in 282 set
snpgdsClose(geno_Hmp)

snpgdsCreateGeno(X.GDS_file,
                 X,
                 sample.id=rownames(X),
                 snp.id=snp_rs,
                 snp.chromosome=map[selected_SNPs]$chromosome,
                 snp.position=map[selected_SNPs]$position,
                 snp.allele=map[selected_SNPs]$allele,
                 snpfirstdim=FALSE)

snpgdsGDS2BED(X.GDS_file, X.BED_prefix)

saveRDS(X, X_file)

# XXt matrix
XXt <- tcrossprod(X)

saveRDS(XXt, XXt_files["genome-wide"])

#########################################################
# Genomic relationships
#########################################################
geno <- snpgdsOpen(X.GDS_file)

# Scaled relatedness (G)
G_list <- snpgdsGRM(geno, method="EIGMIX", num.thread=n_cores)

G <- G_list$grm
rownames(G) <- colnames(G) <- G_list$sample.id

saveRDS(G, G_file)

rm(G_list)
gc()

snpgdsClose(geno)

# Eigendecomposition
ED <- eigen(G)
rownames(ED$vectors) <- rownames(G)

Q <- cbind(1, ED$vectors[, 1:n_PCs] %*% diag(ED$values[1:n_PCs]))
colnames(Q) <- c("(Intercept)", paste("PC", 1:n_PCs, sep=""))

saveRDS(ED, ED_file)
saveRDS(Q, Q_file)
write.table(Q, sub("rds$", "txt", Q_file), row.names=FALSE, col.names=FALSE)

#########################################################
# Linear kernels
#########################################################
# Transcript data
txDb <- makeTxDbFromGFF(GFF_file)

tx_info <- data.frame(transcripts(txDb, use.names=TRUE))  %>%
  mutate_if(is.factor, as.character) %>% 
  filter(seqnames %in% as.character(chromosomes))

tx_names <- tx_info$tx_name

# trans-SNPs
XXt_trans <- setNames(foreach(tx_name=tx_names) %do% {
  
  # SNPs in chromosome
  chr <- tx_info[match(tx_name, tx_info$tx_name), "seqnames"]
  snps_in_chromosome <- map$snp[map$chromosome == chr]
  
  # SNPs in cis region
  tx_range <- IRanges(tx_info[match(tx_name, tx_info$tx_name), "start"] - trans_range,
                      tx_info[match(tx_name, tx_info$tx_name), "end"] + trans_range)
  
  overlapping_snps <- map$snp[countOverlaps(snp_pos, tx_range, type="within") > 0]
  
  # SNPs within range
  snps <- intersect(snps_in_chromosome, overlapping_snps)
  
  fwrite(data.table(snps),
         paste0(cis_SNPs_folder, tx_name, ".txt"),
         sep="\t",
         row.names=FALSE,
         col.names=FALSE,
         quote=FALSE)
  
  # Kernel: whole GRM - GRM within range
  XXt - tcrossprod(X[, snps, drop=FALSE])
  
}, tx_names)

saveRDS(XXt_trans, XXt_files["trans"])

# cis-SNPs
XXt_cis <- setNames(foreach(tx_name=tx_names) %do% {
  
  # SNPs in chromosome
  chr <- tx_info[match(tx_name, tx_info$tx_name), "seqnames"]
  snps_in_chromosome <- map$snp[map$chromosome == chr]
  
  # SNPs in cis region
  tx_range <- IRanges(tx_info[match(tx_name, tx_info$tx_name), "start"] - cis_range,
                      tx_info[match(tx_name, tx_info$tx_name), "end"] + cis_range)
  
  overlapping_snps <- map$snp[countOverlaps(snp_pos, tx_range, type="within") > 0]
  
  # SNPs within range
  snps <- intersect(snps_in_chromosome, overlapping_snps)
  
  # Kernel: GRM within range
  tcrossprod(X[, snps, drop=FALSE])
  
}, tx_names)

saveRDS(XXt_cis, XXt_files["cis"])

# Exporting GRMs for each gene
XXt_list <- list(
  "cis"=XXt_cis,
  "trans"=XXt_trans
)

for (XXt_name in names(XXt_list)) {
  
  GRM_list <- XXt_list[[XXt_name]]
  
  for (gene_name in names(GRM_list)) {
    
    out <- GRM_list[[gene_name]]
    
    saveRDS(out, paste0(XXt_folders[[XXt_name]], gene_name, ".rds"))
    
  }
  
}
