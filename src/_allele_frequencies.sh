#!/usr/bin/env bash
home_dir=~/QGG_plants/

input_dir=${home_dir}snp_constraint_prediction/data/Hmp321/hmp321_unimputed/
output_dir=${home_dir}snp_constraint_prediction/data/Hmp321/

n_chromosomes=10

###################################################################
# Calculating allele frequencies in Hapmap 3.2.1
###################################################################
for i in $(seq 1 1 ${n_chromosomes})
do

echo Chromosome ${i}: Calculating allele frequencies...

sleep 1

vcftools --gzvcf ${input_dir}merged_flt_c${i}.vcf.gz \
--out ${input_dir}Hmp321_AGPv3_chr${i} \
--freq &

done
wait


###################################################################
# Formatting into single table
###################################################################
output_file=${output_dir}Hmp321_AGPv3.frq

echo -e "CHROM_v3\tPOS_v3\tREF\tALT\tAF\tN" > ${output_file}

for i in $(seq 1 1 ${n_chromosomes})
do

echo Chromosome ${i}: Formatting...

tail -n +2 ${input_dir}Hmp321_AGPv3_chr${i}.frq | awk -F'[\t:]' '{print $1 "\t" $2 "\t" $5 "\t" $7 "\t" $6 "\t" $4}' >> ${output_file}

done
