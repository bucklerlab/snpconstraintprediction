# Model testing
# Objective : Evaluating the accuracy of models predicting evolutionary constraint 
# Created by: Guillaume Ramstein (gr226@cornell.edu)
# Created on: 10/31/2019

#--------------------------------------------------------
# Script parameters
#--------------------------------------------------------
# Working directory
wd <- "~/QGG_plants/snp_constraint_prediction/"
dir.create(wd, showWarnings=FALSE)
setwd(wd)

# Input
# annot_files <- c(
#   "NS"="data/annotation/RV_annot_NS.rds",
#   "Reg"="data/annotation/RV_annot_Reg.rds",
#   "Int"="data/annotation/RV_annot_Int.rds"
# )

annot_files <- c(
  "NS"="data/annotation/RV_annot_NS.rds"
)

unirep_file <- "data/annotation/unirep_ref.csv"

proteome_file <- "data/B73/B73_proteome.csv"

# Input parameters
only_expressed <- FALSE

max_distance <- 1000

remove_missing <- TRUE

# Fit parameters
n_max <- 1e7

sample_sizes <- 5e4
n_rep <- 3

include.negative <- FALSE
include.NA <- TRUE

n_trees <- c(50, 100, 200)
learning_rates <- c(0.1, 0.01, 0.001)
min_node_size <- 100

left_out_chromosomes <- c("8")

n_cores <- 20
save.memory <- FALSE

chunk_size <- 1e5

# Variables
y_vars <- c("rs_conservation")

nr_threshold <- 5
rs_threshold <- 0.95

always_included <- list(
  "NS"=c("sift_score", "sift_class", "MUTATION_TYPE"),
  "Reg"=c("REGION"),
  "Int"=c("distance_to_nearest")
)

predictor_list <- list(
  "NS"=list(
    "Base"=always_included[["NS"]],
    "Base+Genomics"=c(always_included[["NS"]],
                      "gc_content", "ltr", "hel", "sline", "tir", "kmer_count"),
    "Base+Genomics+UNIREP_scores"=c(always_included[["NS"]],
                                    "gc_content", "ltr", "hel", "sline", "tir", "kmer_count",
                                    "unirep_DIST", paste0("unirep_D", 1:256)),
    "Base+Genomics+UNIREP_ontology"=c(always_included[["NS"]],
                                      "gc_content", "ltr", "hel", "sline", "tir", "kmer_count",
                                      paste0("unirep_V", 1:256)),
    "Base+Genomics+UNIREP_scores+UNIREP_ontology"=c(always_included[["NS"]],
                                                    "gc_content", "ltr", "hel", "sline", "tir", "kmer_count",
                                                    "unirep_DIST", paste0("unirep_D", 1:256),
                                                    paste0("unirep_V", 1:256))
  ),
  "Reg"=list(
    "Base"=always_included[["Reg"]],
    "Base+Genomics"=c(always_included[["Reg"]],
                      "gc_content", "ltr", "hel", "sline", "tir", "kmer_count"),
    "Base+Genomics+UNIREP_ontology"=c(always_included[["Reg"]],
                                      "gc_content", "ltr", "hel", "sline", "tir", "kmer_count",
                                      paste0("unirep_V", 1:256))
  ),
  "Int"=list(
    "Base"=always_included[["Int"]],
    "Base+Genomics"=c(always_included[["Int"]],
                      "gc_content", "ltr", "hel", "sline", "tir", "kmer_count"),
    "Base+Genomics+UNIREP_ontology"=c(always_included[["Int"]],
                                      "gc_content", "ltr", "hel", "sline", "tir", "kmer_count",
                                      paste0("unirep_V", 1:256))
  )
)

# Output
output_dir <- "data/annotation/model_testing/gradient_boosted_trees/"
dir.create(output_dir, showWarnings=FALSE)

importance_file <- paste0(output_dir, "importance.rds")

result_file <- paste0(output_dir, "fit_results.rds")

#--------------------------------------------------------
# Functions
#--------------------------------------------------------
library(foreach)
library(tidyverse)
library(dplyr)
library(ggplot2)
library(xgboost)
library(data.table)
library(MetricsWeighted)

normalize <- function(x, x_min=NULL, x_max=NULL) {
  
  if (is.null(x_min)) x_min <- min(x, na.rm=TRUE)
  if (is.null(x_max)) x_max <- max(x, na.rm=TRUE)
  
  (x - x_min)/(x_max - x_min)
  
}

down_sample <- function(x, n_max) {
  
  sample(x, size=min(n_max, length(x)), replace=FALSE)
  
}

#########################################################
# Protein information
#########################################################
# Reference Unirep features
unirep <- fread(unirep_file)

# Protein expression
protein_info <- read.csv(proteome_file, row.names=1)

#########################################################
# Analysis by SNP class
#########################################################
res_DF <- data.frame()
imp_DF <- data.frame()

for (SNP_class in names(annot_files)) {
  
  cat("\n\n\n\n\n############################\nInput:",
      SNP_class,
      "\n############################\n")
  
  #------------------------------------------------------
  # Annotation data
  #------------------------------------------------------
  # Full data
  annot <- readRDS(annot_files[SNP_class]) %>%
    dplyr::mutate_if(is.logical, as.numeric) %>%
    dplyr::filter(distance_to_nearest <= max_distance) %>% 
    dplyr::mutate(rs_rate=gerp_Kistler/neutral_Kistler) %>%
    dplyr::mutate(within_1024bp=ifelse(is.na(jasmin_DIFF), 0, 1)) %>% 
    dplyr::mutate(jasmin_DIFF=ifelse(is.na(jasmin_DIFF), 0, jasmin_DIFF),
                  jasmin_DIST=ifelse(is.na(jasmin_DIST), 0, jasmin_DIST)) %>%
    dplyr::mutate(sift_score=ifelse(is.na(sift_score), 1, sift_score)) %>%
    dplyr::mutate(sift_class=factor(ifelse(sift_score <= 0.05, "constrained", "tolerated"))) %>%
    as.data.frame()
  
  if (only_expressed) {
    
    annot <- annot[annot$nearest_gene %in% rownames(protein_info[rowSums(protein_info > 0) >= 5, ]), ]
    
  }
  
  all_predictors <- unique(unlist(predictor_list[[SNP_class]]))
  
  # Conservation level
  annot$rs_conservation <- NA
  
  set.seed(1)
  
  idx_conserved <- which(annot$neutral_Kistler >= nr_threshold & annot$rs_rate >= rs_threshold) %>%
    down_sample(n_max)
  
  idx_neutral <- which(((include.NA & is.na(annot$rs_rate)) | (include.negative & annot$rs_rate < 0))) %>%
    down_sample(n_max)
  
  annot$rs_conservation[idx_conserved] <- 1
  annot$rs_conservation[idx_neutral] <- 0
  
  # Truncated RS scores
  annot$rs_positive <- NA
  
  idx_negative <- which(annot$neutral_Kistler >= nr_threshold & annot$rs_rate <= 0) %>%
    down_sample(n_max)
  
  idx_positive <- which(annot$neutral_Kistler >= nr_threshold & annot$rs_rate > 0) %>%
    down_sample(n_max)
  
  annot$rs_positive[idx_negative] <- 0
  annot$rs_positive[idx_positive] <- annot$rs_rate[idx_positive]
  
  gc()
  
  # Handling missing values at predictors
  x_vars <- intersect(colnames(annot), unique(unlist(predictor_list[[SNP_class]])))
  
  if (remove_missing) {
    
    none_missing <- rowSums(sapply(annot[, x_vars], is.na)) == 0
    annot <- annot[none_missing, ]
    
  } else {
    
    for (j in which(colnames(annot) %in% x_vars & sapply(annot, is.numeric)))  {
      
      annot[, j][is.na(annot[, j])] <- 0
      
    }
    
  }
  
  annot <- annot[, c("CHROM_v3", "nearest_protein", x_vars, y_vars), drop=FALSE]
  
  gc()
  
  #------------------------------------------------------
  # Analysis
  #------------------------------------------------------
  for (y_var in y_vars) {
    
    classification <- length(na.omit(unique(annot[, y_var]))) == 2
    
    annot_obs <- annot[which(! is.na(annot[, y_var])), , drop=FALSE] %>%
      data.table() %>%
      merge(unirep, by.x="nearest_protein", by.y="NAME", sort=FALSE) %>%
      as.data.frame()
    
    n <- nrow(annot_obs)
    
    gc()
    
    # Case weights
    if (classification) {
      
      strata <- interaction(annot_obs$CHROM_v3, annot_obs[, y_var])
      
    } else {
      
      strata <- interaction(annot_obs$CHROM_v3, as.factor(annot_obs[, y_var] > 0))
      
    }
    
    w <- 1 / table(strata)
    
    case.weights <- w[as.character(strata)] / sum(w)
    
    gc()
    
    # Training weights
    CS <- which(! annot_obs$CHROM_v3 %in% left_out_chromosomes)
    
    wtrain <- as.numeric(case.weights)[CS]
    
    min_child_weight <- min_node_size * mean(wtrain)
    
    # Model evaluations
    for (suffix in rev(names(predictor_list[[SNP_class]]))) {
      
      predictors <- predictor_list[[SNP_class]][[suffix]]
      
      f <- as.formula(paste("~", paste(predictors, collapse=" + "), "- 1"))
      
      # Training dataset
      dtrain <- xgb.DMatrix(model.matrix(f, data=annot_obs[CS, ]), label=annot_obs[CS, y_var])
      
      # Validation dataset
      VS <- which(annot_obs$CHROM_v3 %in% left_out_chromosomes)
      
      annot_VS <- annot_obs[VS, c(predictors, y_var), drop=FALSE]
      dtest <- xgb.DMatrix(model.matrix(f, data=annot_VS))
      
      w_VS <- as.numeric(case.weights[VS])
      
      idx_list <- split(1:nrow(annot_VS), cut_width(1:nrow(annot_VS), chunk_size))
      
      y_obs <- annot_VS[ , y_var]
      
      # Model evaluation
      for (sample_size in rev(sample_sizes)) {
        
        for (lr in learning_rates) {
          
          for (n_tree in n_trees) {
            
            for (k in 1:n_rep) {
              
              # Input
              cat("\n\n\n", y_var, "~", suffix, "- n =", sample_size, "- n_tree =", n_tree, "- learning_rate =", lr, "- rep =", k, "\n")
              
              fit_name <- paste0(y_var, ".", SNP_class, ".", suffix, ".n=", sample_size, ".n_tree=", n_tree, ".learning_rate =", lr, ".k=", k)
              fit_file <- paste0(output_dir, fit_name, ".rds")
              
              # Fit
              cat("\tFitting GBT model...\n")
              
              if (file.exists(fit_file)) {
                
                fit <- readRDS(fit_file)
                
              } else {
                
                fit <- xgboost(
                  data=dtrain,
                  missing=NA,
                  weight=wtrain,
                  params=list(
                    booster="gbtree",
                    eta=lr,
                    max_depth=n,
                    min_child_weight=min_child_weight,
                    subsample=sample_size/n,
                    objective="binary:logistic"
                  ),
                  nrounds=n_tree,
                  nthread=n_cores,
                  verbose=1,
                  print_every_n=50,
                  save_period=0,
                  save_name=fit_file
                )
                
                saveRDS(fit, fit_file)
                
              }
              
              gc()
              
              # Prediction accuracy
              if (classification) {
                
                Y_obs <- cbind(y_obs, 1-y_obs)
                colnames(Y_obs) <- c("high", "low")
                
                y_pred <- predict(fit, dtest)
                Y_pred <- cbind(y_pred, 1-y_pred)
                colnames(Y_pred) <- c("high", "low")
                
                confusion_matrix <- crossprod(w_VS * Y_obs, round(Y_pred))
                rownames(confusion_matrix) <- paste0("true_", colnames(Y_obs))
                colnames(confusion_matrix) <- paste0("predicted_", colnames(Y_pred))
                print(confusion_matrix)
                
                print(acc <- sum(diag(confusion_matrix))/sum(confusion_matrix))
                mse <- weighted.mean((y_obs-y_pred)^2, w=w_VS)
                f1 <- f1_score(y_obs, round(y_pred), w=w_VS)
                auc <- AUC(y_obs, round(y_pred), w=w_VS)
                
              } else {
                
                y_pred <- predict(fit, dtest)
                
                print(acc <- cov.wt(cbind(y_obs, y_pred), wt=w_VS, cor=TRUE)$cor[1, 2])
                mse <- weighted.mean((y_obs-y_pred)^2, w=w_VS)
                f1 <- NA
                auc <- NA
                
              }
              
              # Output
              res_DF <- rbind(res_DF,
                              data.frame(
                                SNP_class=SNP_class,
                                n_obs=n,
                                n=sample_size,
                                n_tree=n_tree,
                                lr=lr,
                                k=k,
                                response=y_var,
                                predictors=suffix,
                                type=ifelse(classification, "classification", "regression"),
                                accuracy=acc,
                                mse=mse,
                                f1_score=f1,
                                AUC=auc
                              ))
              
              saveRDS(res_DF, result_file)
              
              rm(fit)
              
              gc()
              
            }
            
            gc()
            
          }
          
          gc()
          
        }
        
        gc()
        
      }
      
      rm(annot_VS)
      
      gc()
      
    }
    
    rm(annot_obs)
    
    gc()
    
  }
  
  rm(annot)
  
  gc()
  
}
