# Title     : Genomic annotations
# Objective : Generate genomic annotations in S. bicolor
# Created by: Guillaume Ramstein (ramstein@qgg.au.dk)

#--------------------------------------------------------
# Script parameters
#--------------------------------------------------------
# Working directory
wd <- "~/QGG_plants/snp_constraint_prediction/data/"
dir.create(wd, showWarnings=FALSE)
setwd(wd)

# Genomic parameters
chromosomes <- as.character(1:10)

# Number of cores
n_cores <- 10

# Input
GFF_file <- "BTx623/Sbicolor_313_v3.1.gene.gff3.gz"
bed_file <- "BTx623/Sbicolor_313_v3.1.gene.bed"

fasta_file <- "BTx623/Sbicolor_313_v3.0.fa.gz"
kcount_file <- "BTx623/kmer_counts/mer_counts_dumps.fa"

vcf_file <- "BTx623/TERRA_SNPs_imputed.vcf.gz"
gds_file <- "BTx623/TERRA_SNPs_imputed.gds"

sift_file <- "BTx623/TERRA_SNPs_imputed_SIFTannotations.txt"

unirep_file <- "BTx623/BTx623_3.31_SNP_Unirep.tsv"

# Output
output_dir <- "annotation/BTx623/"
dir.create(output_dir, showWarnings=FALSE)

annot_file <- paste0(output_dir, "BTx623_annot.rds")
score_file <- paste0(output_dir, "BTx623_unirep_scores.csv")
unirep.ref_file <- paste0(output_dir, "BTx623_unirep_ref.csv")
output_files <- c(
  "NS"=paste0(output_dir, "BTx623_annot_NS.rds"),
  "SS"=paste0(output_dir, "BTx623_annot_SS.rds"),
  "Reg"=paste0(output_dir, "BTx623_annot_Reg.rds"),
  "Int"=paste0(output_dir, "BTx623_annot_Int.rds")
)

CDS_file <- "BTx623/CDS.bed"

#--------------------------------------------------------
# Functions
#--------------------------------------------------------
library(foreach)
library(data.table)
library(tidyverse)
library(dplyr)

library(ggplot2)
library(mgcv)
library(reshape2)

library(Biostrings)
library(GenomicRanges)
library(GenomicFeatures)
library(rtracklayer)
library(SNPRelate)

normalize <- function(x) {
  
  min_x <- min(x, na.rm=TRUE)
  
  (x - min_x)/(max(x, na.rm=TRUE) - min_x)
  
}

#--------------------------------------------------------
# Allele information
#--------------------------------------------------------
# Input
if (! file.exists(gds_file)) {
  
  snpgdsVCF2GDS(vcf_file, gds_file, ignore.chr.prefix="")
  
}

# Allele frequencies
geno_file <- snpgdsOpen(gds_file)

file_split <- sub("^.+/", "", gds_file) %>%
  sub("[.]gds", "", .) %>%
  strsplit("_") %>%
  `[[`(1)

panel <- file_split[1]

# Calculating allele frequencies
annot <- snpgdsSNPList(geno_file) %>% 
  dplyr::select(chromosome, position, allele, afreq) %>%
  data.table

allele_split <- strsplit(annot$allele, "/")
annot$REF <- sapply(allele_split, `[`, 1)
annot$ALT <- sapply(allele_split, `[`, 2)

annot <- dplyr::select(annot, chromosome, position, REF, ALT, afreq) %>%
  dplyr::rename(CHROM=chromosome, POS=position, AF=afreq)

# Closing file
snpgdsClose(geno_file)

# Minor allele frequency from Hapmap 3.2.1
annot$maf <- pmin(annot$AF, 1-annot$AF)

# Output
saveRDS(annot, annot_file)

#--------------------------------------------------------
# k-mer information
#--------------------------------------------------------
cat("kmer counts...\n")

# Genome
genome <- readDNAStringSet(fasta_file)
genome_length <- sum(width(genome)) / 1e6
names(genome) <- gsub(" .+$", "", names(genome))

# k-mer counts
kcount <- readDNAStringSet(kcount_file)
k <- width(kcount)[1]
offsets <- 0:(k-1)

DT <- data.table(kmer=as.character(kcount),
                 count=as.numeric(names(kcount)), 
                 key="kmer")

# Average frequency of k-mers at SNPs
annot$kmer_count <- foreach(offset=offsets, .combine=`+`) %do% {
  
  print(offset)
  
  SNP_intervals <- GRanges(
    seqnames=annot$CHROM,
    ranges=IRanges(start=annot$POS-offset, width=k),
    strand="+")
  
  SNP_sequences <- as.character(BSgenome::getSeq(genome, SNP_intervals))
  
  DT[SNP_sequences]$count
  
} / length(offsets) / genome_length

# GC content
SNP_intervals <- GRanges(
  seqnames=annot$CHROM,
  ranges=IRanges(start=annot$POS-49, width=100),
  strand="+")

SNP_sequences <- BSgenome::getSeq(genome, SNP_intervals)

annot$gc_content <- rowSums(letterFrequency(SNP_sequences, letters=c("C", "G")))

# Output
saveRDS(annot, annot_file)

rm(genome, kcount)
gc()

#--------------------------------------------------------
# SIFT scores
#--------------------------------------------------------
map_columns <- c("CHROM", "POS", "REF", "ALT")

# SIFT scores
annot_SIFT <- fread(sift_file, nThread=n_cores) %>%
  dplyr::select(CHROM, POS, REF_ALLELE, ALT_ALLELE, TRANSCRIPT_ID, REGION,
                REF_AMINO, ALT_AMINO, SIFT_SCORE) %>%
  dplyr::rename(REF=REF_ALLELE, ALT=ALT_ALLELE, 
                AA_REF=REF_AMINO, AA_ALT=ALT_AMINO, sift_score=SIFT_SCORE)

# Mutation type
annot_SIFT$MUTATION_TYPE <- as.character(NA)
annot_SIFT$MUTATION_TYPE[annot_SIFT$AA_REF == annot_SIFT$AA_ALT] <- "SYN"
annot_SIFT$MUTATION_TYPE[annot_SIFT$AA_REF != annot_SIFT$AA_ALT] <- "NON_SYN"
annot_SIFT$MUTATION_TYPE[annot_SIFT$AA_REF == "*" & annot_SIFT$AA_ALT != "*"] <- "STOP_LOSS"
annot_SIFT$MUTATION_TYPE[annot_SIFT$AA_REF != "*" & annot_SIFT$AA_ALT == "*"] <- "STOP_GAIN"

# Merging
annot <- merge(annot, annot_SIFT, by=map_columns, all.x=TRUE, sort=FALSE) %>%
  dplyr::mutate(PROTEIN_ID=ifelse(is.na(TRANSCRIPT_ID), NA, paste0(TRANSCRIPT_ID, ".p")))

# Output
saveRDS(annot, annot_file)

rm(annot_SIFT)
gc()

#--------------------------------------------------------
# CDS bed file
#--------------------------------------------------------
transcripts <- na.omit(unique(annot$TRANSCRIPT_ID))

CDS <- fread(bed_file) %>%
  dplyr::filter(V8 == "CDS") %>%
  dplyr::mutate(TRANSCRIPT_ID = gsub("[.]v3[.]1[.]CDS[.].*", "", V4),
                PROTEIN_ID = paste0(TRANSCRIPT_ID, ".p")) %>%
  dplyr::filter(TRANSCRIPT_ID %in% .GlobalEnv$transcripts) %>%
  dplyr::select(V1, V2, V3, PROTEIN_ID, V5, V6)

write.table(CDS, CDS_file, sep="\t", row.names=FALSE, col.names=FALSE, quote=FALSE)

#--------------------------------------------------------
# UniRep disruption scores
#--------------------------------------------------------
# Representations
unirep <- fread(unirep_file, nThread=n_cores, stringsAsFactors=FALSE, header=FALSE, sep="\t")
unirep <- unirep[! duplicated(unirep[, 1]), ]

unirep_info <- unirep$V1
unirep <- as.matrix(unirep[, -1])
rownames(unirep) <- unirep_info

# SNP information
SNP_idx <- grep("^SNP", unirep_info)

unirep_snp <- unirep[SNP_idx, ]

snp_split <- do.call(rbind, strsplit(unirep_info[SNP_idx], "[_:]"))

snp_DF <- data.frame(CHROM=paste0("Chr", snp_split[, 3]),
                     POS=as.integer(snp_split[, 5]),
                     PROTEIN_ID=snp_split[, 8],
                     stringsAsFactors=FALSE)

# Reference information
ref_idx <- setdiff(1:nrow(unirep), c(SNP_idx, grep("^REF", unirep_info)))
unirep_ref <- unirep[ref_idx, ]
colnames(unirep_ref) <- paste0("unirep_V", 1:ncol(unirep_ref))

reference_exists <- snp_DF$PROTEIN_ID %in% rownames(unirep_ref)

snp_DF <- snp_DF[reference_exists, ]
unirep_snp <- unirep_snp[reference_exists, ]

# Disruption score by CHROM, POS, Protein
D_mat <- unirep_snp - unirep_ref[snp_DF$PROTEIN_ID, ]

score_DF <- cbind(snp_DF,
                  unirep_DIST=sqrt(rowMeans(D_mat**2)),
                  setNames(as.data.frame(D_mat), paste0("unirep_D", 1:ncol(unirep_snp))))

annot.CDS <- merge(annot[which(annot$REGION == "CDS"), ],
                   score_DF,
                   by=c("CHROM", "POS", "PROTEIN_ID"),
                   all.x=TRUE,
                   sort=FALSE)

# Output
unirep_DF <- cbind(data.frame(NAME=rownames(unirep_ref), stringsAsFactors=FALSE), data.frame(unirep_ref))
fwrite(data.table(unirep_DF), unirep.ref_file)

fwrite(score_DF, score_file, quote=FALSE)

saveRDS(annot.CDS[which(annot.CDS$MUTATION_TYPE != "SYN"), ], output_files["NS"])
saveRDS(annot.CDS[which(annot.CDS$MUTATION_TYPE == "SYN"), ], output_files["SS"])
saveRDS(annot[which(annot$REGION %in% c("UTR_3", "UTR_5", "INTRON")), ], output_files["Reg"])
saveRDS(annot[which(annot$REGION == "INTERGENIC"), ], output_files["Int"])
