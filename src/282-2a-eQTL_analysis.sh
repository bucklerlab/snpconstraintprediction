#!/usr/bin/env bash

home_dir=~/QGG_plants/snp_constraint_prediction/data/282set/

bed_prefix=${home_dir}AGPv3_282set
key_file=${bed_prefix}.key

PEER_dir=${home_dir}PEERs/

eQTL_dir=${home_dir}eQTL/

SNP_dir=${home_dir}cis_SNPs/

max_processes=30

if [ ! -d ${eQTL_dir} ]; then
mkdir ${eQTL_dir}
fi

checkpoint=2

###################################################################
# 1. Fitting eQTL models
###################################################################
if [ ${checkpoint} -le 1 ]; then

echo Fitting eQTL models...

n_genes=$(wc -l ${key_file} | cut -d' ' -f 1)

for j in $(seq 1 1 ${n_genes}); do

tissue=$(sed "${j}q;d" ${key_file} | cut -d' ' -f 1)
gene=$(sed "${j}q;d" ${key_file} | cut -d' ' -f 2)
tx=$(sed "${j}q;d" ${key_file} | cut -d' ' -f 3)

# Hold if too many processes
while [ $(pgrep gemma | wc -l) -gt ${max_processes} ]; do

sleep 1

done

# Output directory
output_dir=${eQTL_dir}${tissue}

if [ ! -d ${output_dir} ]; then

mkdir ${output_dir}

fi

cd ${output_dir}

# GLM
if [ ${tx} != "NA" ]; then

gemma \
-bfile ${bed_prefix} \
-c ${PEER_dir}${tissue}_covariates.txt \
-n ${j} \
-maf 0.05 \
-r2 1 \
-lm 1 \
-snps ${SNP_dir}${tx}.txt \
-o ${gene} &

fi

done

wait

fi


###################################################################
# 2. Concatenating results
###################################################################
if [ ${checkpoint} -le 2 ]; then

echo Concatenating results...

tissues=$(cut -d' ' -f 1 ${key_file} | sort | uniq)

for tissue in ${tissues}; do

# Initial file
eQTL_file=${eQTL_dir}${tissue}/assoc.txt

echo -e "gene\trs\tbeta\tse\tp" > ${eQTL_file}

# Concatenation
output_dir=${eQTL_dir}${tissue}/output/

for assoc_file in $(ls ${output_dir} | grep ".assoc.txt$"); do

gene=$(echo ${assoc_file} | sed "s/\.assoc\.txt$//")

cut -d$'\t' -f 2,9,10,11 ${output_dir}${assoc_file} | sed -e '1d' | sed "s/^/${gene}\t/" >> ${eQTL_file}

done

done

fi
