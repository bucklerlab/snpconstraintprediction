#
home_dir=~/QGG_plants/snp_constraint_prediction/

tassel=${home_dir}src/tassel-5-standalone/run_pipeline.pl
vcf_file=${home_dir}data/BTx623/TERRA_SNPs_imputed.vcf.gz
bed_file=${home_dir}data/BTx623/CDS.bed
genome_file=${home_dir}data/BTx623/Sbicolor_313_v3.0.fa.gz
proteome_file=${home_dir}data/BTx623/Sbicolor_313_v3.1.protein.fa

fa_prefix=${home_dir}data/BTx623/BTx623_3.31
fa_file=${fa_prefix}_SNPSpliced.fa

tmp_dir=${home_dir}data/BTx623/tmp
chunk_length=100000

sif_container=${home_dir}src/unirep.sif
py_script=${home_dir}src/predict-on-seqs.py

truncation_length=1000
max_length=2000

output_file=${home_dir}data/BTx623/BTx623_3.31_SNP_Unirep.tsv

# Make SNP protein sequences
${tassel} \
-debug -Xmx50G \
-ExtractSequenceFromVCFPlugin \
-vcfFile ${vcf_file} \
-bedFile ${bed_file} \
-ref ${genome_file} \
-outputFile ${fa_prefix} \
-endPlugin

# Splitting SNP protein files into chunks
if [ ! -d ${tmp_dir} ]; then mkdir ${tmp_dir}; fi

if [ "$(ls -A ${tmp_dir})" ]; then rm ${tmp_dir}/*; fi

split --lines=${chunk_length} ${fa_file} ${tmp_dir}/chunk.

# Calling UniRep on chunks of SNP protein files
if [ -f ${output_file} ]; then rm ${output_file}; fi

for chunk in $(ls ${tmp_dir})

do

echo -e "\n${chunk}\n\n"

singularity exec --nv ${sif_container} python ${py_script} ${tmp_dir}/${chunk} -b 100 -d -t ${truncation_length} -l ${max_length} >> ${output_file}

done

rm ${tmp_dir}/*

# Calling UniRep on reference protein file
echo -e "\nReference proteins\n\n"

singularity exec --nv ${sif_container} python ${py_script} ${proteome_file} -b 100 -t ${truncation_length} -l ${max_length} >> ${output_file}
