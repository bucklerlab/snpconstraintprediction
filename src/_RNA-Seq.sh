# Hai Wang, 2018 Oct 20, RNA-Seq analysis of 10 samples, using cbsuem01
#################################################################################################################################
# set working directory and trim reads
#################################################################################################################################
mkdir /workdir/hw449
cd /workdir/hw449

# copy fastq files 
scp -r hw449@cbsublfs1.tc.cornell.edu:/data1/RawSeqData/RNASeq/andropogoneae/protein_level_prediction/C202SC18041648/raw_data/ .
cd /workdir/hw449/raw_data

# Quality trimming
~/sickle/sickle pe \
-f ./B73_1/B73_1_USR17008485L_HM3M3BBXX_L2_1.fq.gz -r ./B73_1/B73_1_USR17008485L_HM3M3BBXX_L2_2.fq.gz \
-o B73_1_1.trimmed.fastq.gz -p B73_1_2.trimmed.fastq.gz -s B73_1_s.trimmed.fastq.gz \
-t sanger -g >>B73_1.olog 2>>B73_1.elog &

~/sickle/sickle pe \
-f ./B73_2/B73_2_USR17008490L_HM3M3BBXX_L2_1.fq.gz -r ./B73_2/B73_2_USR17008490L_HM3M3BBXX_L2_2.fq.gz \
-o B73_2_1.trimmed.fastq.gz -p B73_2_2.trimmed.fastq.gz -s B73_2_s.trimmed.fastq.gz \
-t sanger -g >>B73_2.olog 2>>B73_2.elog &

~/sickle/sickle pe \
-f ./Mo17_1/Mo17_1_USR17008486L_HM3M3BBXX_L2_1.fq.gz -r ./Mo17_1/Mo17_1_USR17008486L_HM3M3BBXX_L2_2.fq.gz \
-o Mo17_1_1.trimmed.fastq.gz -p Mo17_1_2.trimmed.fastq.gz -s Mo17_1_s.trimmed.fastq.gz \
-t sanger -g >>Mo17_1.olog 2>>Mo17_1.elog & 

~/sickle/sickle pe \
-f ./Mo17_2/Mo17_2_USR17008491L_HM3M3BBXX_L2_1.fq.gz -r ./Mo17_2/Mo17_2_USR17008491L_HM3M3BBXX_L2_2.fq.gz \
-o Mo17_2_1.trimmed.fastq.gz -p Mo17_2_2.trimmed.fastq.gz -s Mo17_2_s.trimmed.fastq.gz \
-t sanger -g >>Mo17_2.olog 2>>Mo17_2.elog &

~/sickle/sickle pe \
-f ./CML_1/CML_1_USR17008487L_HM3M3BBXX_L2_1.fq.gz -r ./CML_1/CML_1_USR17008487L_HM3M3BBXX_L2_2.fq.gz \
-o CML247_1_1.trimmed.fastq.gz -p CML247_1_2.trimmed.fastq.gz -s CML247_1_s.trimmed.fastq.gz \
-t sanger -g >>CML247_1.olog 2>>CML247_1.elog & 

~/sickle/sickle pe \
-f ./CML_2/CML_2_USR17008492L_HM3M3BBXX_L2_1.fq.gz -r ./CML_2/CML_2_USR17008492L_HM3M3BBXX_L2_2.fq.gz \
-o CML247_2_1.trimmed.fastq.gz -p CML247_2_2.trimmed.fastq.gz -s CML247_2_s.trimmed.fastq.gz \
-t sanger -g >>CML247_2.olog 2>>CML247_2.elog &

~/sickle/sickle pe \
-f ./Yugu_1/Yugu_1_USR17008489L_HM3M3BBXX_L2_1.fq.gz -r ./Yugu_1/Yugu_1_USR17008489L_HM3M3BBXX_L2_2.fq.gz \
-o Yugu1_1_1.trimmed.fastq.gz -p Yugu1_1_2.trimmed.fastq.gz -s Yugu1_1_s.trimmed.fastq.gz \
-t sanger -g >>Yugu1_1.olog 2>>Yugu1_1.elog & 

~/sickle/sickle pe \
-f ./Yugu_2/Yugu_2_USR17008494L_HM3M3BBXX_L2_1.fq.gz -r ./Yugu_2/Yugu_2_USR17008494L_HM3M3BBXX_L2_2.fq.gz \
-o Yugu1_2_1.trimmed.fastq.gz -p Yugu1_2_2.trimmed.fastq.gz -s Yugu1_2_s.trimmed.fastq.gz \
-t sanger -g >>Yugu1_2.olog 2>>Yugu1_2.elog &

~/sickle/sickle pe \
-f ./Btx_1/Btx_1_USR17008488L_HM3M3BBXX_L2_1.fq.gz -r ./Btx_1/Btx_1_USR17008488L_HM3M3BBXX_L2_2.fq.gz \
-o BTx623_1_1.trimmed.fastq.gz -p BTx623_1_2.trimmed.fastq.gz -s BTx623_1_s.trimmed.fastq.gz \
-t sanger -g >>BTx623_1.olog 2>>BTx623_1.elog & 

~/sickle/sickle pe \
-f ./Btx_2/Btx_2_USR17008493L_HM3M3BBXX_L2_1.fq.gz -r ./Btx_2/Btx_2_USR17008493L_HM3M3BBXX_L2_2.fq.gz \
-o BTx623_2_1.trimmed.fastq.gz -p BTx623_2_2.trimmed.fastq.gz -s BTx623_2_s.trimmed.fastq.gz \
-t sanger -g >>BTx623_2.olog 2>>BTx623_2.elog &

##################################################################################################################################
# Genomes and indices
##################################################################################################################################
# B73_v3
# download maize V3 genome file and annotation file to directory maize_genome
mkdir /workdir/hw449/B73_v3_genome
cd /workdir/hw449/B73_v3_genome
wget ftp://ftp.ensemblgenomes.org/pub/plants/release-31/gtf/zea_mays/Zea_mays.AGPv3.31.gtf.gz
wget ftp://ftp.ensemblgenomes.org/pub/plants/release-31/fasta/zea_mays/dna/Zea_mays.AGPv3.31.dna.toplevel.fa.gz
gunzip *.gz
# build index for B73
source /programs/HISAT2/hisat2.sh
extract_splice_sites.py Zea_mays.AGPv3.31.gtf > B73_v3.ss
extract_exons.py Zea_mays.AGPv3.31.gtf > B73_v3.exon
hisat2-build -p 20 --ss B73_v3.ss --exon B73_v3.exon Zea_mays.AGPv3.31.dna.toplevel.fa B73_v3_index &

# B73_v4
# download maize V4 genome file and annotation file to directory maize_genome
mkdir /workdir/hw449/B73_genome
cd /workdir/hw449/B73_genome
wget ftp://ftp.ensemblgenomes.org/pub/plants/release-41/gtf/zea_mays/Zea_mays.B73_RefGen_v4.41.gtf.gz
wget ftp://ftp.ensemblgenomes.org/pub/plants/release-41/fasta/zea_mays/dna/Zea_mays.B73_RefGen_v4.dna.toplevel.fa.gz
gunzip *.gz
# build index for B73
source /programs/HISAT2/hisat2.sh
extract_splice_sites.py Zea_mays.B73_RefGen_v4.41.gtf > B73.ss
extract_exons.py Zea_mays.B73_RefGen_v4.41.gtf > B73.exon
hisat2-build -p 20 --ss B73.ss --exon B73.exon Zea_mays.B73_RefGen_v4.dna.toplevel.fa B73_index &

# sorghum
# cp sorghum V3 genome and annotation  
mkdir /workdir/hw449/sorghum_genome
cd /workdir/hw449/sorghum_genome
cp ~/Genomes/sorghum_V3/Sbicolor_454_v3.1.1.gene_exons.gff3 .
cp ~/Genomes/sorghum_V3/Sbicolor_454_v3.0.1.fa .
# build index for sorghum
~/gffread/gffread/gffread Sbicolor_454_v3.1.1.gene_exons.gff3 -T -o sorghum.gtf
source /programs/HISAT2/hisat2.sh
extract_splice_sites.py sorghum.gtf > sorghum.ss
extract_exons.py sorghum.gtf > sorghum.exon
hisat2-build -p 20 --ss sorghum.ss --exon sorghum.exon Sbicolor_454_v3.0.1.fa sorghum_index &

# Setaria italica
# cp Yugu1 genome and annotation
mkdir /workdir/hw449/Yugu1_genome
cd /workdir/hw449/Yugu1_genome
cp ~/Genomes/Setaria_italica/Sitalica_312_v2.fa .
cp ~/Genomes/Setaria_italica/Sitalica_312_v2.2.gene_exons.gff3 .
# build index for Yugu1
~/gffread/gffread/gffread Sitalica_312_v2.2.gene_exons.gff3 -T -o Yugu1.gtf
source /programs/HISAT2/hisat2.sh
extract_splice_sites.py Yugu1.gtf > Yugu1.ss
extract_exons.py Yugu1.gtf > Yugu1.exon
hisat2-build -p 20 --ss Yugu1.ss --exon Yugu1.exon Sitalica_312_v2.fa Yugu1_index &

# Mo17_CAU
mkdir /workdir/hw449/Mo17_CAU_genome
cd /workdir/hw449/Mo17_CAU_genome
wget https://ftp.maizegdb.org/MaizeGDB/FTP/Zm-Mo17-REFERENCE-CAU-1.0/Zm-Mo17-REFERENCE-CAU-1.0.fa.gz
wget https://ftp.maizegdb.org/MaizeGDB/FTP/Zm-Mo17-REFERENCE-CAU-1.0/Zm00014a.gff3.gz
gunzip *.gz
~/gffread/gffread/gffread Zm00014a.gff3 -T -o Mo17_CAU.gtf
source /programs/HISAT2/hisat2.sh
extract_splice_sites.py Mo17_CAU.gtf > Mo17_CAU.ss
extract_exons.py Mo17_CAU.gtf > Mo17_CAU.exon
hisat2-build -p 20 --ss Mo17_CAU.ss --exon Mo17_CAU.exon Zm-Mo17-REFERENCE-CAU-1.0.fa Mo17_CAU_index &

# CML247_1.0
mkdir /workdir/hw449/CML247_1.0_genome
cd /workdir/hw449/CML247_1.0_genome
wget https://ftp.maizegdb.org/MaizeGDB/FTP/Zm-CML247-DRAFT-PANZEA-1.0/Zm-CML247-DRAFT-PANZEA-1.0.fasta.gz
wget https://ftp.maizegdb.org/MaizeGDB/FTP/Zm-CML247-DRAFT-PANZEA-1.0/Zm00006a.gff
gunzip *.gz
~/gffread/gffread/gffread Zm00006a.gff -T -o CML247_1.0.gtf
source /programs/HISAT2/hisat2.sh
extract_splice_sites.py CML247_1.0.gtf > CML247_1.0.ss
extract_exons.py CML247_1.0.gtf > CML247_1.0.exon
hisat2-build -p 30 --ss CML247_1.0.ss --exon CML247_1.0.exon Zm-CML247-DRAFT-PANZEA-1.0.fasta CML247_1.0_index &


################################################################################################################################
# map reads to genomes
################################################################################################################################
cd /workdir/hw449
source /programs/HISAT2/hisat2.sh

# B73
hisat2 \
-x B73_genome/B73_index -1 raw_data/B73_1_1.trimmed.fastq.gz -2 raw_data/B73_1_2.trimmed.fastq.gz \
-S B73_1.sam -p 10 --dta >>B73_1.hisat2.olog 2>>B73_1.hisat2.elog &
hisat2 \
-x B73_genome/B73_index -1 raw_data/B73_2_1.trimmed.fastq.gz -2 raw_data/B73_2_2.trimmed.fastq.gz \
-S B73_2.sam -p 10 --dta >>B73_2.hisat2.olog 2>>B73_2.hisat2.elog &

# B73_v3
hisat2 \
-x B73_v3_genome/B73_v3_index -1 raw_data/B73_1_1.trimmed.fastq.gz -2 raw_data/B73_1_2.trimmed.fastq.gz \
-S B73_v3_1.sam -p 10 --dta >>B73_v3_1.hisat2.olog 2>>B73_v3_1.hisat2.elog &
hisat2 \
-x B73_v3_genome/B73_v3_index -1 raw_data/B73_2_1.trimmed.fastq.gz -2 raw_data/B73_2_2.trimmed.fastq.gz \
-S B73_v3_2.sam -p 10 --dta >>B73_v3_2.hisat2.olog 2>>B73_v3_2.hisat2.elog &

# Sorghum
hisat2 \
-x sorghum_genome/sorghum_index -1 raw_data/BTx623_1_1.trimmed.fastq.gz -2 raw_data/BTx623_1_2.trimmed.fastq.gz \
-S BTx623_1.sam -p 10 --dta >>BTx623_1.hisat2.olog 2>>BTx623_1.hisat2.elog &
hisat2 \
-x sorghum_genome/sorghum_index -1 raw_data/BTx623_2_1.trimmed.fastq.gz -2 raw_data/BTx623_2_2.trimmed.fastq.gz \
-S BTx623_2.sam -p 10 --dta >>BTx623_2.hisat2.olog 2>>BTx623_2.hisat2.elog &

# Yugu1
hisat2 \
-x Yugu1_genome/Yugu1_index -1 raw_data/Yugu1_1_1.trimmed.fastq.gz -2 raw_data/Yugu1_1_2.trimmed.fastq.gz \
-S Yugu1_1.sam -p 10 --dta >>Yugu1_1.hisat2.olog 2>>Yugu1_1.hisat2.elog &
hisat2 \
-x Yugu1_genome/Yugu1_index -1 raw_data/Yugu1_2_1.trimmed.fastq.gz -2 raw_data/Yugu1_2_2.trimmed.fastq.gz \
-S Yugu1_2.sam -p 10 --dta >>Yugu1_2.hisat2.olog 2>>Yugu1_2.hisat2.elog &

# Mo17_CAU
hisat2 \
-x Mo17_CAU_genome/Mo17_CAU_index -1 raw_data/Mo17_1_1.trimmed.fastq.gz -2 raw_data/Mo17_1_2.trimmed.fastq.gz \
-S Mo17_CAU_1.sam -p 10 --dta >>Mo17_CAU_1.hisat2.olog 2>>Mo17_CAU_1.hisat2.elog &
hisat2 \
-x Mo17_CAU_genome/Mo17_CAU_index -1 raw_data/Mo17_2_1.trimmed.fastq.gz -2 raw_data/Mo17_2_2.trimmed.fastq.gz \
-S Mo17_CAU_2.sam -p 10 --dta >>Mo17_CAU_2.hisat2.olog 2>>Mo17_CAU_2.hisat2.elog &

# CML247_1.0
hisat2 \
-x CML247_1.0_genome/CML247_1.0_index -1 raw_data/CML247_1_1.trimmed.fastq.gz -2 raw_data/CML247_1_2.trimmed.fastq.gz \
-S CML247_1.0_1.sam -p 10 --dta >>CML247_1.0_1.hisat2.olog 2>>CML247_1.0_1.hisat2.elog &
hisat2 \
-x CML247_1.0_genome/CML247_1.0_index -1 raw_data/CML247_2_1.trimmed.fastq.gz -2 raw_data/CML247_2_2.trimmed.fastq.gz \
-S CML247_1.0_2.sam -p 10 --dta >>CML247_1.0_2.hisat2.olog 2>>CML247_1.0_2.hisat2.elog &

################################################################################################################################
# SAM to BAM
################################################################################################################################
samtools sort -@ 10 -o B73_1.sorted.bam B73_1.sam  >>B73_1.samtools.olog 2>>B73_1.samtools.elog &
samtools sort -@ 10 -o B73_2.sorted.bam B73_2.sam  >>B73_2.samtools.olog 2>>B73_2.samtools.elog &
samtools sort -@ 10 -o BTx623_1.sorted.bam BTx623_1.sam  >>BTx623_1.samtools.olog 2>>BTx623_1.samtools.elog &
samtools sort -@ 10 -o BTx623_2.sorted.bam BTx623_2.sam  >>BTx623_2.samtools.olog 2>>BTx623_2.samtools.elog &
samtools sort -@ 10 -o Yugu1_1.sorted.bam Yugu1_1.sam  >>Yugu1_1.samtools.olog 2>>Yugu1_1.samtools.elog &
samtools sort -@ 10 -o Yugu1_2.sorted.bam Yugu1_2.sam  >>Yugu1_2.samtools.olog 2>>Yugu1_2.samtools.elog &
samtools sort -@ 10 -o B73_v3_1.sorted.bam B73_v3_1.sam  >>B73_v3_1.samtools.olog 2>>B73_v3_1.samtools.elog &
samtools sort -@ 10 -o B73_v3_2.sorted.bam B73_v3_2.sam  >>B73_v3_2.samtools.olog 2>>B73_v3_2.samtools.elog &
samtools sort -@ 10 -o Mo17_CAU_1.sorted.bam Mo17_CAU_1.sam  >>Mo17_CAU_1.samtools.olog 2>>Mo17_CAU_1.samtools.elog &
samtools sort -@ 10 -o Mo17_CAU_2.sorted.bam Mo17_CAU_2.sam  >>Mo17_CAU_2.samtools.olog 2>>Mo17_CAU_2.samtools.elog &
samtools sort -@ 10 -o CML247_1.0_1.sorted.bam CML247_1.0_1.sam  >>CML247_1.0_1.samtools.olog 2>>CML247_1.0_1.samtools.elog &
samtools sort -@ 10 -o CML247_1.0_2.sorted.bam CML247_1.0_2.sam  >>CML247_1.0_2.samtools.olog 2>>CML247_1.0_2.samtools.elog &

# After everything is done...
rm *.sam
mkdir map_reads_to_genomes
mv *.elog *.olog *.bam map_reads_to_genomes/

################################################################################################################################
# Quantify gene expression levels
################################################################################################################################
# B73
~/subread-1.6.0-Linux-x86_64/bin/featureCounts -Q 10 -F GTF -T 1 -t exon -g gene_id \
-a ./B73_genome/Zea_mays.B73_RefGen_v4.41.gtf -o counts_FC.B73_1 ./map_reads_to_genomes/B73_1.sorted.bam \
>>B73_1.fc.olog 2>>B73_1.fc.elog &

~/subread-1.6.0-Linux-x86_64/bin/featureCounts -Q 10 -F GTF -T 1 -t exon -g gene_id \
-a ./B73_genome/Zea_mays.B73_RefGen_v4.41.gtf -o counts_FC.B73_2 ./map_reads_to_genomes/B73_2.sorted.bam \
>>B73_2.fc.olog 2>>B73_2.fc.elog &

# Sorghum
~/subread-1.6.0-Linux-x86_64/bin/featureCounts -Q 10 -F GTF -T 1 -t exon -g gene_id \
-a ./sorghum_genome/sorghum.gtf -o counts_FC.BTx623_1 ./map_reads_to_genomes/BTx623_1.sorted.bam \
>>BTx623_1.fc.olog 2>>BTx623_1.fc.elog &

~/subread-1.6.0-Linux-x86_64/bin/featureCounts -Q 10 -F GTF -T 1 -t exon -g gene_id \
-a ./sorghum_genome/sorghum.gtf -o counts_FC.BTx623_2 ./map_reads_to_genomes/BTx623_2.sorted.bam \
>>BTx623_2.fc.olog 2>>BTx623_2.fc.elog &

# Yugu1
~/subread-1.6.0-Linux-x86_64/bin/featureCounts -Q 10 -F GTF -T 1 -t exon -g gene_id \
-a ./Yugu1_genome/Yugu1.gtf -o counts_FC.Yugu1_1 ./map_reads_to_genomes/Yugu1_1.sorted.bam \
>>Yugu1_1.fc.olog 2>>Yugu1_1.fc.elog &

~/subread-1.6.0-Linux-x86_64/bin/featureCounts -Q 10 -F GTF -T 1 -t exon -g gene_id \
-a ./Yugu1_genome/Yugu1.gtf -o counts_FC.Yugu1_2 ./map_reads_to_genomes/Yugu1_2.sorted.bam \
>>Yugu1_2.fc.olog 2>>Yugu1_2.fc.elog &

# B73_v3
~/subread-1.6.0-Linux-x86_64/bin/featureCounts -Q 10 -F GTF -T 1 -t exon -g gene_id \
-a ./B73_v3_genome/Zea_mays.AGPv3.31.gtf -o counts_FC.B73_v3_1 ./map_reads_to_genomes/B73_v3_1.sorted.bam \
>>B73_v3_1.fc.olog 2>>B73_v3_1.fc.elog &

~/subread-1.6.0-Linux-x86_64/bin/featureCounts -Q 10 -F GTF -T 1 -t exon -g gene_id \
-a ./B73_v3_genome/Zea_mays.AGPv3.31.gtf -o counts_FC.B73_v3_2 ./map_reads_to_genomes/B73_v3_2.sorted.bam \
>>B73_v3_2.fc.olog 2>>B73_v3_2.fc.elog &

# Mo17_CAU
~/subread-1.6.0-Linux-x86_64/bin/featureCounts -Q 10 -F GTF -T 1 -t exon -g gene_id \
-a ./Mo17_CAU_genome/Mo17_CAU.gtf -o counts_FC.Mo17_CAU_1 ./map_reads_to_genomes/Mo17_CAU_1.sorted.bam \
>>Mo17_CAU_1.fc.olog 2>>Mo17_CAU_1.fc.elog &

~/subread-1.6.0-Linux-x86_64/bin/featureCounts -Q 10 -F GTF -T 1 -t exon -g gene_id \
-a ./Mo17_CAU_genome/Mo17_CAU.gtf -o counts_FC.Mo17_CAU_2 ./map_reads_to_genomes/Mo17_CAU_2.sorted.bam \
>>Mo17_CAU_2.fc.olog 2>>Mo17_CAU_2.fc.elog &

# CML247_1.0
~/subread-1.6.0-Linux-x86_64/bin/featureCounts -Q 10 -F GTF -T 1 -t exon -g gene_id \
-a ./CML247_1.0_genome/CML247_1.0.gtf -o counts_FC.CML247_1.0_1 ./map_reads_to_genomes/CML247_1.0_1.sorted.bam \
>>CML247_1.0_1.fc.olog 2>>CML247_1.0_1.fc.elog &

~/subread-1.6.0-Linux-x86_64/bin/featureCounts -Q 10 -F GTF -T 1 -t exon -g gene_id \
-a ./CML247_1.0_genome/CML247_1.0.gtf -o counts_FC.CML247_1.0_2 ./map_reads_to_genomes/CML247_1.0_2.sorted.bam \
>>CML247_1.0_2.fc.olog 2>>CML247_1.0_2.fc.elog &

# B73
# stringtie complains about Mt and Pt genes. 
grep -v -E "^Mt|^Pt" B73_genome/Zea_mays.B73_RefGen_v4.41.gtf > B73_genome/Zea_mays.B73_RefGen_v4.41.filtered.gtf

~/stringtie-1.3.3b.Linux_x86_64/stringtie \
./map_reads_to_genomes/B73_1.sorted.bam -G ./B73_genome/Zea_mays.B73_RefGen_v4.41.filtered.gtf -A B73_1.tab -e -o B73_1.gtf >>B73_1.st.olog 2>>B73_1.st.elog &

~/stringtie-1.3.3b.Linux_x86_64/stringtie \
./map_reads_to_genomes/B73_2.sorted.bam -G ./B73_genome/Zea_mays.B73_RefGen_v4.41.filtered.gtf -A B73_2.tab -e -o B73_2.gtf >>B73_2.st.olog 2>>B73_2.st.elog &

# Sorghum
~/stringtie-1.3.3b.Linux_x86_64/stringtie \
./map_reads_to_genomes/BTx623_1.sorted.bam -G ./sorghum_genome/sorghum.gtf -A BTx623_1.tab -e -o BTx623_1.gtf >>BTx623_1.st.olog 2>>BTx623_1.st.elog &

~/stringtie-1.3.3b.Linux_x86_64/stringtie \
./map_reads_to_genomes/BTx623_2.sorted.bam -G ./sorghum_genome/sorghum.gtf -A BTx623_2.tab -e -o BTx623_2.gtf >>BTx623_2.st.olog 2>>BTx623_2.st.elog &

# Yugu1
~/stringtie-1.3.3b.Linux_x86_64/stringtie \
./map_reads_to_genomes/Yugu1_1.sorted.bam -G ./Yugu1_genome/Yugu1.gtf -A Yugu1_1.tab -e -o Yugu1_1.gtf >>Yugu1_1.st.olog 2>>Yugu1_1.st.elog &

~/stringtie-1.3.3b.Linux_x86_64/stringtie \
./map_reads_to_genomes/Yugu1_2.sorted.bam -G ./Yugu1_genome/Yugu1.gtf -A Yugu1_2.tab -e -o Yugu1_2.gtf >>Yugu1_2.st.olog 2>>Yugu1_2.st.elog &

# B73_v3
# stringtie complains about zma-MIR genes. 
grep -v "zma-MIR" B73_v3_genome/Zea_mays.AGPv3.31.gtf > B73_v3_genome/Zea_mays.AGPv3.31.filtered.gtf

~/stringtie-1.3.3b.Linux_x86_64/stringtie \
./map_reads_to_genomes/B73_v3_1.sorted.bam -G ./B73_v3_genome/Zea_mays.AGPv3.31.filtered.gtf -A B73_v3_1.tab -e -o B73_v3_1.gtf >>B73_v3_1.st.olog 2>>B73_v3_1.st.elog &

~/stringtie-1.3.3b.Linux_x86_64/stringtie \
./map_reads_to_genomes/B73_v3_2.sorted.bam -G ./B73_v3_genome/Zea_mays.AGPv3.31.filtered.gtf -A B73_v3_2.tab -e -o B73_v3_2.gtf >>B73_v3_2.st.olog 2>>B73_v3_2.st.elog &

# Mo17_CAU
~/stringtie-1.3.3b.Linux_x86_64/stringtie \
./map_reads_to_genomes/Mo17_CAU_1.sorted.bam -G ./Mo17_CAU_genome/Mo17_CAU.gtf -A Mo17_CAU_1.tab -e -o Mo17_CAU_1.gtf >>Mo17_CAU_1.st.olog 2>>Mo17_CAU_1.st.elog &

~/stringtie-1.3.3b.Linux_x86_64/stringtie \
./map_reads_to_genomes/Mo17_CAU_2.sorted.bam -G ./Mo17_CAU_genome/Mo17_CAU.gtf -A Mo17_CAU_2.tab -e -o Mo17_CAU_2.gtf >>Mo17_CAU_2.st.olog 2>>Mo17_CAU_2.st.elog &

# CML247_1.0
~/stringtie-1.3.3b.Linux_x86_64/stringtie \
./map_reads_to_genomes/CML247_1.0_1.sorted.bam -G ./CML247_1.0_genome/CML247_1.0.gtf -A CML247_1.0_1.tab -e -o CML247_1.0_1.gtf >>CML247_1.0_1.st.olog 2>>CML247_1.0_1.st.elog &

~/stringtie-1.3.3b.Linux_x86_64/stringtie \
./map_reads_to_genomes/CML247_1.0_2.sorted.bam -G ./CML247_1.0_genome/CML247_1.0.gtf -A CML247_1.0_2.tab -e -o CML247_1.0_2.gtf >>CML247_1.0_2.st.olog 2>>CML247_1.0_2.st.elog &

mkdir quantification
mv *.elog *.olog *.gtf *.tab counts_FC.* quantification/
