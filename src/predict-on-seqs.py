import argparse
import logging
from math import ceil
from pathlib import Path
from pprint import pformat
import sys
# this is the path to the GitHub repo in the Singularity container
sys.path.append('/unirep/UniRep')
from tempfile import NamedTemporaryFile

from Bio import SeqIO
from Bio.Alphabet import generic_dna, generic_protein
import numpy as np
import tensorflow as tf
# use the 256-vector version of UniRef
from unirep import babbler256 as babbler

parser = argparse.ArgumentParser(
    prog = 'python predict-on-seqs.py'
)
parser.add_argument(
    'seqs',
    metavar = 'seqs.fa',
    help = 'path to the FASTA file containing the protein sequences to be run through UniRep'
)
parser.add_argument(
    '-b', '--batch-size',
    metavar = 'INT',
    default = 128,
    type = int,
    help = 'size of batches to feed into UniRep for prediction'
)
parser.add_argument(
    '-d', '--dna',
    action = 'store_true',
    help = 'input is DNA, not protein, so needs to be translated first'
)
parser.add_argument(
    '-t', '--truncate-to',
    metavar = 'INT',
    type = int,
    help = 'number of amino acids to truncate protein sequences to'
)
parser.add_argument(
    '-l', '--length-filter',
    metavar = 'INT',
    type = int,
    default = 274,
    help = 'drop all amino acids longer than this amino acid length cutoff'
)
args = parser.parse_args()
logging.basicConfig(level = logging.DEBUG)
logging.debug(pformat(args))

if args.length_filter > 274:
    logging.warning("Length filter is greater than 274, you may run into GPU memory problems")

if not args.truncate_to or (args.truncate_to > 274):
    logging.warning("Truncation length is greater than 274, you may run into GPU memory problems")

# random seed makes a determinate batch, which is the length of the longest sequence **in the batch**
# changing the random seed results in a different shuffle, potentially having one of the larger sequences in it
#tf.set_random_seed(42)
#np.random.seed(42)

# path to the model weights in the singularity container
model_weight_path = "/unirep/weights/256_weights"

if args.dna:
    dna_seqs = SeqIO.parse(args.seqs, 'fasta', alphabet = generic_dna)
    seqs = (dna_seq.translate(id = True, to_stop = True) for dna_seq in dna_seqs)
else:
    # protein sequence provided
    seqs = (rec.upper() for rec in SeqIO.parse(args.seqs, 'fasta', alphabet = generic_protein))

batch_size = args.batch_size

b = babbler(batch_size = batch_size, model_path = model_weight_path)

# unfortunately UniRep requires this to be written to a file
# the output vectors are in the same order as the input sequences
seq_ids = []
frmt_file = NamedTemporaryFile()
lengthy_seqs = 0
invalid_seqs = 0
valid_seqs = 0
valid_AAs = set("MRHKDESTNQCUGPAVIFYWLO")
with open(frmt_file.name, 'w') as dest:
    for seq in seqs:
        # remove stop codons if present
        seq.seq = seq.seq.rstrip('*')
        if args.truncate_to:
            seq = seq[:args.truncate_to]
            logging.debug("{} truncated".format(seq.id))
        if len(seq) > args.length_filter:
            lengthy_seqs += 1
            logging.debug("{} too long".format(seq.id))
            continue
        if b.is_valid_seq(seq, max_len = args.length_filter + 1):
            valid_seqs += 1
            formatted = ','.join(map(str, b.format_seq(seq.seq)))
            dest.write(formatted)
            seq_ids.append(seq.id)
            dest.write("\n")
        else:
            invalid_seqs += 1
            invalid_AAs = set(seq.seq) - valid_AAs
            if not invalid_AAs:
                logging.warning("Empty invalid amino acids but sequence {} filtered: {}".format(seq.id, seq.seq))
            logging.debug("{} contains invalid characters: {}".format(seq.id, invalid_AAs))
logging.warning("Kept {} valid sequences, skipped {} invalid and {} lengthy sequences".format(valid_seqs, invalid_seqs, lengthy_seqs))

def tf_seq_to_tensor(s):
    return tf.string_to_number(
        tf.sparse_tensor_to_dense(tf.string_split([s], ','), default_value = '0'), out_type = tf.int32
        )[0]

def pad_batch_op(formatted_seqs_path, batch_size):
    batch_size = tf.constant(batch_size, tf.int64)
    ds = tf.contrib.data.TextLineDataset(formatted_seqs_path).map(tf_seq_to_tensor)
    return ds.padded_batch(batch_size, ([None]))

# generate the Tensorflow operation to bucket and pad the formatted sequences
bucket_op = pad_batch_op(frmt_file.name, batch_size).make_one_shot_iterator().get_next()

# grab more Tensorflow ops for later
final_hidden, x_placeholder, batch_size_placeholder, seq_length_placeholder, initial_state_placeholder = (b.get_rep_ops())

"""
initializer = tf.contrib.layers.xavier_initializer(uniform = False)
with tf.variable_scope('top'):
    predictions = tf.contrib.layers.fully_connected(
        final_hidden, 1, activation_fn = None,
        weights_initializer = initializer,
        biases_initializer = tf.zeros_initializer()
    )
"""

#this is required to extract the correct vectors, see the Jupyter notebook in the repo
def nonpad_len(batch):
    nonzero = batch > 0
    lengths = np.sum(nonzero, axis = 1)
    return lengths

batch_cnt = 1
total_batches = ceil(valid_seqs / batch_size)
with tf.Session() as sess:
    small_last_batch = False
    while True:
        sess.run(tf.global_variables_initializer())
        # grab our batch
        try:
            batch = sess.run(bucket_op)
        except tf.errors.OutOfRangeError:
            break
        logging.debug("Processing batch {}/{}".format(batch_cnt, total_batches))
        if batch.shape[0] < batch_size:
            # we are on the last batch and it is smaller than the others
            # therefore we need to pad it to the batch size or else errors in the graph occur
            num_to_pad = batch_size - batch.shape[0]
            padded_vals = np.zeros((num_to_pad, batch.shape[1]))
            batch = np.concatenate([batch, padded_vals], axis = 0)
            small_last_batch = True
        length = nonpad_len(batch)

        # calculate the values of the final hidden layer and store them
        protein_vectors = sess.run(
            final_hidden,
            feed_dict = {
                x_placeholder: batch,
                batch_size_placeholder: batch_size,
                seq_length_placeholder: length,
                initial_state_placeholder: b._zero_state
            }
        )

        if small_last_batch:
            # last batch, need to take off the useless zero predictions we added earlier
            protein_vectors = protein_vectors[:-num_to_pad, :]
        
        try:
            all_prot_vecs = np.concatenate([all_prot_vecs, protein_vectors])
        except NameError:
            # first batch, need to define the array
            all_prot_vecs = protein_vectors
        batch_cnt += 1

for i, seq_id in enumerate(seq_ids):
    print("{}\t{}".format(seq_id, "\t".join([str(e) for e in all_prot_vecs[i, :]])))
