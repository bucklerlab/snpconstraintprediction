# Title     : Expression input
# Objective : Making input to CNN models
# Created by: Guillaume Ramstein
# Created on: 8/14/19

#########################################################
# Script parameters
#########################################################
# Working directory
wd <- "~/QGG_plants/snp_constraint_prediction/"
dir.create(wd, showWarnings=FALSE)
setwd(wd)

# Input
NW_file <- "data/G5/proteome_alignment/all_by_all_GRamstein_NW_all_results.out"

info_file <- "data/G5/proteome_alignment/protein_info.txt"

gff_dir <- "data/G5/GFF"

taxa <- c("B73_AGPv3", "Mo17_Zm00014a", "BTx623_v3.1.1", "Yugu1_v2.2")

gff_files <- paste(sep="/", gff_dir,
                   c("Zea_mays.AGPv3.31.gtf.gz",
                     "Zm00014a.gff3.gz",
                     "Sbicolor_454_v3.1.1.gene.gff3.gz",
                     "Sitalica_312_v2.2.gene.gff3.gz")
)
names(gff_files) <- taxa

RNAseq_folder <- "data/G5/RNAseq/"

gene_columns <- c("B73_v3"=1,
                  "Mo17_CAU"=1,
                  "BTx623"=2,
                  "Yugu1"=2)

# Output
out_file <- "data/G5/TPM_data.csv"

#########################################################
# Functions
#########################################################
library(ggplot2)
library(dplyr)
library(GenomicFeatures)
library(Biostrings)
library(data.table)
library(foreach)

#########################################################
# Representative gene model for each gene
#########################################################
# Protein info
protein_info <- read.table(info_file, header=TRUE, stringsAsFactors=FALSE)

# NW results
nw <- fread(NW_file, stringsAsFactor=FALSE, nThread=12)

nw <- as.data.frame(nw) %>%
   dplyr::filter(evalue < 0.001,
                 bitscore > 50,
                 query %in% rownames(protein_info),
                 subject %in% rownames(protein_info)) %>%
   dplyr::group_by(query, subject) %>%
   dplyr::summarise(sbitscore=sum(bitscore)) %>%
   as.data.frame

nw$qtxid <- as.character(protein_info[nw$query, 'tx'])
nw$qgeneid <- as.character(protein_info[nw$query, 'gene'])

nw$stxid <- as.character(protein_info[nw$subject, 'tx'])
nw$sgeneid <- as.character(protein_info[nw$subject, 'gene'])

# Add transcript info to NW
tx_rlen_all <- foreach(taxon=taxa, .combine=c) %do% {
   
   txdb <- makeTxDbFromGFF(gff_files[taxon])
   
   tx <- transcripts(txdb, use.names=TRUE)
   
   setNames(width(tx), names(tx))
   
}

nw$qtxrlen <- tx_rlen_all[nw$qtxid]
nw$stxrlen <- tx_rlen_all[nw$stxid]

# Filtering
nw_flt <- nw %>% 
   dplyr::filter(qgeneid != sgeneid) %>% 
   dplyr::group_by(qgeneid, sgeneid) %>% 
   dplyr::arrange(desc(sbitscore), desc(qtxrlen), desc(stxrlen)) %>% 
   dplyr::slice(1) %>%
   as.data.frame

nw_info <- rbind(
   setNames(nw_flt[, c("qgeneid", "qtxid")], c("gene", "tx")),
   setNames(nw_flt[, c("sgeneid", "stxid")], c("gene", "tx"))) %>% 
   dplyr::group_by(gene, tx) %>% 
   dplyr::summarise(n=n()) %>% 
   dplyr::group_by(gene) %>% 
   dplyr::arrange(desc(n)) %>% 
   dplyr::slice(1) %>%
   as.data.frame

###################################################################################################################
# Getting log(1+TPM) for each selected transcript
###################################################################################################################
get_expr <- function(species, gene_column) {
   
   RNASEQ1 <- read.delim(paste0(RNAseq_folder, species, "_1.tab"),
                         header=TRUE,
                         sep="\t",
                         stringsAsFactor=FALSE)
   
   RNASEQ2 <- read.delim(paste0(RNAseq_folder, species, "_2.tab"),
                         header=TRUE,
                         sep="\t",
                         stringsAsFactor=FALSE)
   
   genes <- sort(unique(c(RNASEQ1[, gene_column], RNASEQ2[, gene_column])))
   
   RNASEQ <- data.frame(taxon=species,
                        gene=genes,
                        r1=RNASEQ1[match(genes, RNASEQ1[, gene_column]), 'TPM'],
                        r2=RNASEQ2[match(genes, RNASEQ2[, gene_column]), 'TPM'],
                        stringsAsFactors=FALSE)
   
   RNASEQ$logTPM <- log10(rowMeans(RNASEQ[, c("r1", "r2")])+1)
   
   return(RNASEQ)
   
}

TPM_DF <- foreach(taxon=names(gene_columns), .combine=rbind) %do% {
   get_expr(taxon, gene_columns[taxon])
}

###################################################################################################################
# Transcript information
###################################################################################################################
# Representative transcript for each gene
TPM_DF$tx <- as.character(nw_info[match(TPM_DF$gene, nw_info$gene), "tx"])

protein_info$tx_rlen <- tx_rlen_all[protein_info$tx]

longest <- protein_info %>% 
   dplyr::group_by(gene) %>% 
   dplyr::arrange(desc(tx_rlen)) %>% 
   dplyr::slice(1) %>%
   as.data.frame
rownames(longest) <- longest$gene

TPM_DF$tx_longest <- as.character(longest[match(TPM_DF$gene, longest$gene), "tx"])
TPM_DF$tx_full <- ifelse(is.na(TPM_DF$tx), TPM_DF$tx_longest, TPM_DF$tx)
TPM_DF <- TPM_DF[!is.na(TPM_DF$tx_full), ]

# Output
write.csv(TPM_DF, out_file, quote=FALSE, row.names=FALSE)
