# Title     : Genomic annotations
# Objective : Import genomic annotations from MonetDB in CDS
# Created by: Guillaume Ramstein (gr226@cornell.edu)
# Created on: 04/06/2020

#--------------------------------------------------------
# Script parameters
#--------------------------------------------------------
# Working directory
wd <- "~/QGG_plants/snp_constraint_prediction/"
dir.create(wd, showWarnings=FALSE)
setwd(wd)

# Genomic parameters
chromosomes <- as.character(1:10)

maf_threshold <- 0.01

nr_threshold <- 5
rs_threshold <- 0.95

# Input
annot_file <- "data/annotation/annot_CDS.rds"

GFF_file <- "data/G5/GFF/Zea_mays.AGPv3.31.chr.gff3.gz"
eggnog_file <- "data/eggnog/Zea_mays.AGPv3.31.pep.all.experimental_GO_terms.emapper.annotations"

bed_file <- "data/G5/transcript.chr.filtered.bed"
info_file <- "data/G5/proteome_alignment/protein_info.txt"
TPM_file <- "data/G5/TPM_data.csv"

synteny_file <- "data/annotation/synteny_report.txt"
protein_file <- "data/B73/B73_proteome.csv"
RNA_file <- "data/B73/B73_transcriptome.csv"

GOslim_file <- "data/annotation/goslim_plant.obo"

unirep_file <- "data/annotation/unirep_ref.csv"

v3v4_file <- "data/annotation/gene_model_xref_v4.txt"

# Input parameters
GO_categories <- c("MF", "BP", "CC")

# Output
output_dir <- "data/annotation/"

gene_file <- paste0(output_dir, "gene_info.rds")

#--------------------------------------------------------
# Functions
#--------------------------------------------------------
library(foreach)
library(data.table)
library(tidyverse)
library(dplyr)

library(ggplot2)
library(mgcv)
library(reshape2)

library(ontologyIndex)
library(GO.db)

library(GenomicRanges)
library(GenomicFeatures)
library(Biostrings)

library(SNPRelate)

#--------------------------------------------------------
# Genomic annotations
#--------------------------------------------------------
# Protein ID information
protein_annot <- read.table(info_file, header=TRUE, stringsAsFactors=FALSE)
protein_to_gene <- setNames(protein_annot$gene, protein_annot$protein)

# SNP annotations
annot <- readRDS(annot_file) %>%
  dplyr::mutate(MAF=pmin(af_Hmp321, 1-af_Hmp321),
                rs_rate=gerp_Kistler/neutral_Kistler)

monomorphic <- with(annot, is.na(af_Hmp321) | MAF < maf_threshold)

# Gene annotations
gene_info <- unique(annot[, c("CHROM_v3", "nearest_gene", "nearest_protein")]) %>%
  dplyr::rename(chromosome=CHROM_v3,
                gene=nearest_gene,
                protein=nearest_protein) %>%
  dplyr::mutate(protein_length=.GlobalEnv$protein_annot[protein, "protein_len"]) %>%
  as.data.frame()

# eggNOG output
eggnog <- read.delim(eggnog_file, skip=3, stringsAsFactors=FALSE) %>%
  dplyr::rename(query_name=X.query_name) %>%
  dplyr::filter(query_name %in% .GlobalEnv$gene_info$protein)

# AGPv4 gene models
v3v4 <- read.delim(v3v4_file, skip=4)

duplicated_v3 <- v3v4$v3_gene_model[duplicated(v3v4$v3_gene_model)]
duplicated_v4 <- v3v4$v4_gene_model[duplicated(v3v4$v4_gene_model)]
one_to_one <- ! (v3v4$v3_gene_model %in% duplicated_v3 | v3v4$v4_gene_model %in% duplicated_v4)

v3v4 <- v3v4[one_to_one, ]

gene_info <- merge(gene_info,
                   v3v4[, c("v3_gene_model", "v4_gene_model", "v4_locus")],
                   by.x="gene",
                   by.y="v3_gene_model",
                   all.x=TRUE,
                   sort=FALSE)

#--------------------------------------------------------
# Evolutionary information
#--------------------------------------------------------
# Polymorphism information
gene_info$Pn <- table(annot[which(! monomorphic & annot$MUTATION_TYPE != "SYN"), "nearest_gene"])[gene_info$gene] %>%
  as.numeric() %>%
  replace_na(0)

gene_info$Ps <- table(annot[which(! monomorphic & annot$MUTATION_TYPE == "SYN"), "nearest_gene"])[gene_info$gene] %>%
  as.numeric() %>%
  replace_na(0)

gene_info <- dplyr::mutate(gene_info,
                           P_prop=Pn/(Pn+Ps))

# Synteny information
synteny_info <- read.table(synteny_file, header=TRUE, stringsAsFactors=FALSE)

gene_info <- merge(gene_info,
                   synteny_info[, ! colnames(synteny_info) %in% c("Zm_tx")],
                   by.x="gene",
                   by.y="Zm_gene",
                   all.x=TRUE,
                   sort=FALSE)

gene_info$is_syntenic <- (!is.na(gene_info$Sb_tx))
gene_info$is_duplicated[is.na(gene_info$is_duplicated)] <- FALSE

# GERP information by gene
deleterious <- with(annot, neutral_Kistler >= nr_threshold & rs_rate >= rs_threshold)

var_list <- list(
  "deleterious_SNPs"=replace(deleterious, which(monomorphic), FALSE)
)

for (var_name in names(var_list)) {
  
  gene_info[, var_name] <- tapply(var_list[[var_name]], annot$nearest_gene, sum, na.rm=TRUE)[gene_info$gene] %>%
    as.numeric()
  
}

# Orthologs
gene_info$ortholog <- eggnog$seed_eggNOG_ortholog[match(gene_info$protein, eggnog$query_name)]

ortholog_count <- table(eggnog$seed_eggNOG_ortholog)
gene_info$copy_number <- as.numeric(ortholog_count[gene_info$ortholog])

#--------------------------------------------------------
# Expression information
#--------------------------------------------------------
# RNA expression
RNA_info <- fread(RNA_file)
RNA_idx <- which(! colnames(RNA_info) %in% c("Gene_ID", "biotype", "gene_set", "synteny"))
colnames(RNA_info)[RNA_idx] <- paste0("RNA_", colnames(RNA_info)[RNA_idx])

gene_info <- merge(gene_info,
                   RNA_info,
                   by.x="gene",
                   by.y="Gene_ID",
                   all.x=TRUE,
                   sort=FALSE)

# Protein expression
protein_info <- fread(protein_file)
protein_idx <- which(! colnames(protein_info) %in% c("Gene_ID", "biotype", "gene_set", "synteny"))
colnames(protein_info)[protein_idx] <- paste0("proteome_", colnames(protein_info)[protein_idx])

gene_info <- merge(gene_info,
                   protein_info,
                   by.x="gene",
                   by.y="Gene_ID",
                   all.x=TRUE,
                   sort=FALSE)

# Gene expression summaries
for (prefix in c("proteome_", "RNA_")) {
  
  X <- log(1 + as.matrix(gene_info[, grep(prefix, colnames(gene_info))]))
  
  gene_info[, paste0(prefix, "min")] <- apply(X, 1, min)
  gene_info[, paste0(prefix, "max")] <- apply(X, 1, max)
  gene_info[, paste0(prefix, "median")] <- apply(X, 1, median)
  gene_info[, paste0(prefix, "SD")] <- apply(X, 1, sd)
  gene_info[, paste0(prefix, "tissues")] <- apply(X > 0, 1, sum)
  
}

#--------------------------------------------------------
# GO ontology
#--------------------------------------------------------
# GO terms
GO_DF <- as.data.frame(GOTERM)
GO_DF <- GO_DF[, c("go_id", "Term", "Ontology", "Definition")]
GO_DF <- GO_DF[!duplicated(GO_DF), ]

GO_ancestors <- c(
  as.list(GOBPANCESTOR),
  as.list(GOMFANCESTOR),
  as.list(GOCCANCESTOR)
)

GO_offspring <- c(
  as.list(GOBPOFFSPRING),
  as.list(GOMFOFFSPRING),
  as.list(GOCCOFFSPRING)
)

# GO slim terms
GO_slim <- get_ontology(GOslim_file)
to_keep <- intersect(which(! GO_slim$obsolete), grep("GO", names(GO_slim$obsolete)))

GO_slim <- lapply(GO_slim, function(dataset) {
  dataset[to_keep]
})

slim_IDs <- intersect(GO_slim$id, GO_DF$go_id)

# eggNOG GO terms
gene_info <- merge(gene_info,
                   dplyr::filter(eggnog[, c("query_name", "GOs")], GOs != ""),
                   by.x="protein",
                   by.y="query_name",
                   all.x=TRUE,
                   sort=FALSE)

# Mapping GO annotations to GO slim IDs
GO_list <- strsplit(gene_info$GOs, ',')

slim_DF <- sapply(GO_list, function(gene_GO) {
  
  if (is.na(gene_GO)) {
    
    as.numeric(rep(NA, length(slim_IDs)))
    
  } else {
    
    gene_slim <- intersect(
      slim_IDs,
      c(gene_GO, unlist(GO_ancestors[gene_GO]))
    )
    
    as.numeric(slim_IDs %in% gene_slim)
    
  }
  
}) %>%
  t() %>%
  as.data.frame() %>%
  setNames(slim_IDs)

gene_info <- cbind(gene_info, slim_DF)

# UniRep features
unirep <- fread(unirep_file) %>%
  data.frame(row.names=1)

unirep <- unirep[intersect(unique(annot$nearest_protein), rownames(unirep)), ]

unirep$gene <- protein_to_gene[rownames(unirep)]

gene_info <- merge(gene_info,
                   unirep,
                   by="gene",
                   all.x=TRUE,
                   sort=FALSE)

#--------------------------------------------------------
# Output
#--------------------------------------------------------
saveRDS(gene_info, gene_file)
