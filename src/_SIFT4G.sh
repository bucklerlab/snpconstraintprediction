#!/usr/bin/env bash

sift_dir=/workdir/gr226/SIFT4G/scripts_to_build_SIFT_db/

export PATH=/usr/local/gcc-7.3.0/bin:$PATH
export LD_LIBRARY_PATH=/usr/local/gcc-7.3.0/lib64:/usr/local/gcc-7.3.0/lib

cd ${sift_dir}
nohup perl make-SIFT-db-all.pl -config Zea_mays.AGPv3.31.txt &
