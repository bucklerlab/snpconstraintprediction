# Instructions to run EggNOG from docker
home_dir=~/QGG_plants/

EGGNOG_DATA=${home_dir}snp_constraint_prediction/data/eggnog
cd $EGGNOG_DATA

INPUT_FASTA=${home_dir}snp_constraint_prediction/data/G5/proteome_alignment/Zea_mays.AGPv3.31.pep.all.fa
OUTPUT_FILE=${home_dir}snp_constraint_prediction/data/eggnog/Zea_mays.AGPv3.31.pep.all

n_cores=60

# Download the DB and proteins and unzip them into a directory
wget -nH --user-agent=Mozilla/5.0 --relative --no-parent --reject "index.html*" --cut-dirs=4 -e robots=off -O eggnog.db.gz http://eggnogdb.embl.de/download/emapperdb-5.0.0/eggnog.db.gz
gunzip eggnog.db.gz
wget -nH --user-agent=Mozilla/5.0 --relative --no-parent --reject "index.html*" --cut-dirs=4 -e robots=off -O eggnog_proteins.dmnd.gz http://eggnogdb.embl.de/download/emapperdb-5.0.0/eggnog_proteins.dmnd.gz
gunzip eggnog_proteins.dmnd.gz

# Install the docker
docker1 build -t eggnog $EGGNOG_DATA

# Run the Docker in interactive mode
# docker1 run --rm -v $EGGNOG_DATA:/usr/local/lib/python2.7/site-packages/data -it biohpc_gr226/eggnog

# emapper: all GO terms
nohup docker1 run --rm -v $EGGNOG_DATA:/usr/local/lib/python2.7/site-packages/data \
-t biohpc_gr226/eggnog /emapper.py \
--excluded_taxa 4577 \
--query-cover 80 --subject-cover 80 \
-i $INPUT_FASTA --output $OUTPUT_FILE \
-m diamond --cpu ${n_cores} &

# emapper: experimental GO terms
nohup docker1 run --rm -v $EGGNOG_DATA:/usr/local/lib/python2.7/site-packages/data \
-t biohpc_gr226/eggnog /emapper.py \
--excluded_taxa 4577 \
--query-cover 80 --subject-cover 80 \
--go_evidence experimental \
--matrix BLOSUM80 \
-i $INPUT_FASTA --output ${OUTPUT_FILE}.experimental_GO_terms \
-m diamond --cpu ${n_cores} &
