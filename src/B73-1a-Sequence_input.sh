#!/usr/bin/env bash

home_dir=~/QGG_plants/snp_constraint_prediction/data/G5/
gff_dir=${home_dir}GFF/
fasta_dir=${home_dir}fasta/

vcf_dir=~/QGG_plants/snp_constraint_prediction/data/282set/
vcf_prefix=c
vcf_suffix=_282_onHmp321.vcf.gz
n_chromosomes=10

declare -a taxa=("B73_AGPv3" "Mo17_Zm00014a" "BTx623_v3.1.1" "Yugu1_v2.2")

declare -a features=("three_prime_UTR" "five_prime_UTR" "transcript")


#------------------------------------------------------------------
# Data
#------------------------------------------------------------------
# GFF files
declare -A gff_files=(["B73_AGPv3"]="Zea_mays.AGPv3.31.chr.gff3.gz" ["Mo17_Zm00014a"]="Zm00014a.gff3.gz" ["BTx623_v3.1.1"]="Sbicolor_454_v3.1.1.gene.gff3.gz" ["Yugu1_v2.2"]="Sitalica_312_v2.2.gene.gff3.gz")

declare -A gene_columns=(["B73_AGPv3"]="10" ["Mo17_Zm00014a"]="9" ["BTx623_v3.1.1"]="10" ["Yugu1_v2.2"]="10")

# Genome sequences
declare -A fasta_files=(["B73_AGPv3"]="Zea_mays.AGPv3.31.dna.genome.fa.gz" ["Mo17_Zm00014a"]="Zm-Mo17-REFERENCE-CAU-1.0.fa.gz" ["BTx623_v3.1.1"]="Sbicolor_454_v3.0.1.fa.gz" ["Yugu1_v2.2"]="Sitalica_312_v2.fa.gz")

gene_list=$(printf "%s-" "${taxa[@]}" | sed -e "s/-$/.genes.txt/")

fasta_output=$(printf "%s-" "${taxa[@]}" | sed -e "s/-$/.fa/")


#------------------------------------------------------------------
# Interval file
#------------------------------------------------------------------
# List of genes for all features
if [ -f ${home_dir}${gene_list} ]
then

rm ${home_dir}${gene_list}

fi

# bed file by feature
for feature in "${features[@]}"
do

# Removing pre-existing file
if [ -f ${home_dir}${feature}.bed ]
then

rm ${home_dir}${feature}.bed

fi

# Making bed output
for taxon in "${taxa[@]}"
do

gff_file=${gff_dir}${gff_files[${taxon}]}

gunzip -c ${gff_file} | \
gff2bed | \
cut -f1,2,3,5,6,7,8,9,10 | \
sed 's/P00rime/prime/' | \
sed 's/mRNA/transcript/' | \
awk -F'[\t;:]' -v gene_column=${gene_columns[${taxon}]} -v taxon=${taxon} -v feature=${feature} '
    BEGIN {
        OFS="\t"
        }
    $7 ~ feature {
        sub(/\.v.+/, "", $gene_column)
        sub(/Parent=/, "", $gene_column)
        sub(/ID=/, "", $gene_column)
        sub(/Name=/, "", $gene_column)
        print taxon"_"$1, $2, $3, $gene_column, $4, $5
    }' >> ${home_dir}${feature}.bed

done

# Only shared annotations at nuclear chromosomes
grep -E 'Mt|Pt' -v ${home_dir}${feature}.bed | sort -u -k4 > ${home_dir}${feature}.chr.bed

if [ ! -f ${home_dir}${gene_list} ]
then

awk -F'\t' '{print $4}' ${home_dir}${feature}.chr.bed > ${home_dir}${gene_list}

else

awk -F'\t' '{print $4}' ${home_dir}${feature}.chr.bed >> ${home_dir}${gene_list}

echo "$(sort ${home_dir}${gene_list} | uniq -d)" > ${home_dir}${gene_list}

fi

done


#------------------------------------------------------------------
# Intersection of unique genes in bed files
#------------------------------------------------------------------
for feature in "${features[@]}"
do

grep -wFf ${home_dir}${gene_list} ${home_dir}${feature}.chr.bed > ${home_dir}${feature}.chr.filtered.bed

done

#------------------------------------------------------------------
# Fasta file
#------------------------------------------------------------------
# Removing pre-existing file
if [ -f ${home_dir}${fasta_output} ]
then

rm ${home_dir}${fasta_output}

fi

# Making combined fasta output
for taxon in "${taxa[@]}"
do

fasta_file=${fasta_dir}${fasta_files[${taxon}]}

gunzip -c ${fasta_file} | sed '/^>/ s/ .*//' | sed "s/>/>${taxon}_/" >> ${home_dir}${fasta_output}

done

# Replacing ambiguous bases to N
sed -i '/^[^>]/s/[R|Y|W|S|M|K|H|B|V|D]/N/g' ${home_dir}${fasta_output}

# Indexing
samtools faidx ${home_dir}${fasta_output}

#------------------------------------------------------------------
# SNP data
#------------------------------------------------------------------
cd ${vcf_dir}

if [ -f ${vcf_prefix}tmp_files.txt ]
then

rm tmp_files.txt

fi

# Only SNPs for B73 in Hapmap3.2.1 VCF files
for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

vcftools --gzvcf ${vcf_prefix}${i}${vcf_suffix} \
--indv 282set_B73 \
--remove-indels \
--min-alleles 2 \
--max-alleles 2 \
--recode \
--out B73_AGPv3_chr${i} &

done
wait

# Indexing files
for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

bgzip -f B73_AGPv3_chr${i}.recode.vcf &

echo B73_AGPv3_chr${i}.recode.vcf.gz >> tmp_files.txt

done
wait

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

tabix B73_AGPv3_chr${i}.recode.vcf.gz &

done
wait

# Concatenating VCF files
bcftools concat -f tmp_files.txt -Oz -o B73_AGPv3.vcf.gz

# Removing temporary files
for tmp_file in $(cat tmp_files.txt); do rm ${tmp_file}; done

rm ${vcf_prefix}tmp_files.txt
