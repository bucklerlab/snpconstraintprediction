# Title     : Testing significance of synthetic expression variables
# Objective : Create G, Q, X and Y
# Created by: Guillaume Ramstein (gr226@cornell.edu)
# Created on: 1/30/19

#--------------------------------------------------------
# Script parameters
#--------------------------------------------------------
# Working directory
wd <- "~/QGG_plants/snp_constraint_prediction"
dir.create(wd, showWarnings = FALSE)
setwd(wd)

# Input
tissues <- c("L3Base", "L3Tip", "L3", "GShoot", "GRoot")

Q_file <- "data/282set/Q.rds"

GRM_file <- "data/282set/GRMs/XXt.rds"

XXt_folders <- c("cis"="data/282set/GRMs/cis/",
                 "trans"="data/282set/GRMs/trans/")

vscount_file <- "data/282set/expression/VST_counts.rds"

PEER_folder <- "data/282set/PEERs"

new_ID <- c(
  "CI7"="Ci7",
  "CI90C"="Ci90C",
  "CI91B"="Ci91B",
  "DE2"="DE-2",
  "Il101T"="Il101",
  "Il677a"="i677a",
  "IllHy"="ILLHy",
  "Ki3"="KI3",
  "Ky226"="KY226",
  "Ky228"="KY228",
  "Mo1W"="MO1W",
  "Oh7B"="OH7B",
  "SG18"="Sg18",
  "Va102"="VA102",
  "W117HT"="W117Ht",
  "Yu796NS"="Yu796"
)

# Output
VC_file <- "data/282set/VC_results.rds"

# Parallelization
n_cores <- 1

# CV parameters
n_CV <- 1
n_fold <- 5

#--------------------------------------------------------
# Functions
#--------------------------------------------------------
# Functions
library(data.table)
library(foreach)
library(glmnet)
library(regress)
library(plyr)
library(doParallel)
library(DESeq2)
library(ggplot2)

kmAIC = function(fit) {
  
  m = ncol(fit$centers)
  n = length(fit$cluster)
  k = nrow(fit$centers)
  D = fit$tot.withinss
  
  return(
    data.frame(
      AIC = D + 2*m*k,
      BIC = D + log(n)*m*k)
  )
  
}

VC_fit <- function(fixed, random) {
  
  # Initialization
  fit <- try(regress(formula=fixed, Vformula=random))
  
  if (class(fit) == "try-error") {
    
    out <- NA
    
  } else {
    
    # Constrained fit
    fit <- try(regress(formula=fixed,
                       Vformula=random,
                       pos=rep(TRUE, length(fit$sigma)),
                       start=pmax(0, fit$sigma)))
    
    if (class(fit) == "try-error") {
      
      out <- NA
      
    } else {
      
      out <- fit
      
    }
    
  }
  
  return(out)
  
}

# Parallelization over runs
if (n_cores > 1) {
  
  cl <- makeCluster(n_cores)
  registerDoParallel(cl)
  parallel::mcaffinity(1:n_cores)
  
  clusterCall(cl, function(pkgs) {
    for (lib in pkgs) {
      library(lib, character.only = TRUE)
    }
  }, loadedNamespaces())
  
}

#########################################################
# Input
#########################################################
# Expression data
vscount <- readRDS(vscount_file)[tissues]

E_list <- setNames(foreach(tissue=tissues) %do% {
  t(assay(vscount[[tissue]]))
}, tissues)

genes <- Reduce(intersect, lapply(E_list, colnames))
taxa <- Reduce(intersect, lapply(E_list, rownames))

# PEER <- as.matrix(read.table(PEER_file, header=TRUE, row.names=1))
# rownames(PEER) <- gsub("282set_", "", gsub("Goodman-Buckler", "", rownames(PEER)))

# Population structure
Q <- readRDS(Q_file)
Q <- Q[, apply(Q, 2, var) > 0]

GRM <- readRDS(GRM_file)
GRM <- GRM/mean(diag(GRM))
rownames(GRM) <- colnames(GRM) <- gsub("282set_", "", gsub("Goodman-Buckler", "", rownames(GRM)))

# Matching names
taxa <- Reduce(intersect, append(list(taxa), lapply(list(Q, GRM), rownames)))
n <- length(taxa)

Q <- Q[taxa,]
GRM <- GRM[taxa, taxa]

# Subsetting on genes with available information
XXt_genes <- lapply(XXt_folders, function(folder) gsub("[.]rds$", "", list.files(folder, pattern="[.]rds$")))
genes <- Reduce(intersect, append(list(genes), XXt_genes))

#########################################################
# Fold assignment
#########################################################
# k-means clustering
AIC_seq <- sapply(1:10, function(k) {
  kmAIC(kmeans(Q, centers=k, iter.max=1e4, nstart=1000))$AIC
})

n_centers <- which.min(AIC_seq)

km <- kmeans(Q, centers=n_centers, iter.max=1e4, nstart=1000)

plot(Q[,1], Q[,2], xlab="PC1", ylab="PC2", col=rainbow(n_centers)[km$cluster])
plot(Q[,2], Q[,3], xlab="PC2", ylab="PC3", col=rainbow(n_centers)[km$cluster])

# CV fold assignment within clusters
fold_list <- list()

for (r in 1:n_CV) {
  
  out <- integer(n)
  set.seed(r)
  
  for (k in 1:n_centers) {
    
    cluster_size <- sum(km$cluster == k)
    
    out[km$cluster == k] <- sample(rep(1:n_fold, each=ceiling(cluster_size/n_fold)))[1:cluster_size]
    
  }
  
  fold_list[[r]] <- out
  
}

# Fold assignment by cluster
fold_list[[n_CV+1]] <- km$cluster

#########################################################
# Variance partition
#########################################################
# Projecting against covariates
X <- matrix(1, nrow=n, ncol=1)
H <- diag(n) - X %*% solve(crossprod(X)) %*% t(X)

# cis/trans variation
VC <- foreach(tissue=tissues, .combine=rbind) %do% {
  
  E <- E_list[[tissue]][taxa, genes]
  
  foreach(gene=genes, .combine=rbind) %do% {
    
    y <- E[, gene]
    
    # GRM
    G_list <- setNames(foreach(XXt_name=names(XXt_folders)) %do% {
      
      out <- readRDS(paste0(XXt_folders[[XXt_name]], gene, ".rds"))
      
      out <- out[taxa, taxa]
      
      out <- H %*% out %*% H
      
      return(out)
      
    }, names(XXt_folders))
    
    G_list[["all"]] <- Reduce("+", G_list)
    
    for (G_type in names(G_list)) {
      
      G_list[[G_type]] <- G_list[[G_type]]/mean(diag(G_list[[G_type]]))
      
    }
    
    # Null fit
    fit0 <- VC_fit(y ~ 1, ~ G_list[["all"]])
    
    LL0 <- ifelse(all(is.na(fit0)), NA, fit0$llik)
    
    # cis/trans partition
    fit1 <- VC_fit(y ~ 1, ~ G_list[["cis"]] + G_list[["trans"]])
    
    LL1 <- ifelse(all(is.na(fit1)), NA, fit1$llik)
    p <- pchisq(2*(LL1-LL0), df=1, lower.tail=FALSE)
    
    # Variance components
    if (all(is.na(fit0))) {
      
      s2 <- rep(NA, 3)
      
    } else {
      
      if (all(is.na(fit1))) {
        
        s2 <- c(NA, fit0$sigma)
        
      } else {
        
        s2 <- fit1$sigma
        
      }
      
    }
    
    # Output
    out <- data.frame(tissue=tissue,
                      gene=gene,
                      s2_cis=s2[1],
                      s2_trans=s2[2],
                      s2_e=s2[3],
                      p=p)
    
    rownames(out) <- NULL
    
    return(out)
    
  }
  
}

saveRDS(VC, VC_file)

