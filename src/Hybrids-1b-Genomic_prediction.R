# Title     : Burden analysis
# Objective : Assess the effect of mutational burden in NAM
# Created by: Guillaume Ramstein (gr226@cornell.edu)
# Created on: 11/29/2018

#--------------------------------------------------------
# Script parameters
#--------------------------------------------------------
overwrite <- FALSE

# Working directory 
wd <- "~/QGG_plants/snp_constraint_prediction/"
dir.create(wd, showWarnings = FALSE)
setwd(wd)

# Input data
input_folder <- "data/hybrids/"

VS_folders <- c("NAM_H"="data/NAM_H/",
                "Ames_H"="data/Ames_H/")

NAM_folder <- "data/NAM_H/"

SNP_classes <- c("NS")

# Analyses
Q_variables <- c("PC1", "PC2", "PC3")

quantiles <- list(
    "sift_score"=c(0, 0.5, 0.9, 0.99, 0.999),
    "rs_conservation"=c(0, 0.5, 0.9, 0.99, 0.999),
    "rs_conservation.obs"=0,
    "base"=list(NULL)
)

fit_G <- TRUE

n_cores <- 1

n_perm <- 20

traits <- c(
  "GY_adjusted",
  "PH",
  "DTS"
)

# Output
model_dir <- paste0(input_folder, "model_fitting/")
dir.create(model_dir, showWarnings=FALSE)

output_file <- paste0(input_folder, "CV.rds")

plot_legend <- c(
  "GY_adjusted"="GY",
  "PH"="PH",
  "DTS"="DTS",
  "sift_score"="SIFT score",
  "rs_conservation"="Predicted PNC",
  "rs_conservation.obs"="Observed PNC",
  "base"="Baseline"
)

#--------------------------------------------------------
# Functions
#--------------------------------------------------------
library(foreach)
library(data.table)
library(reshape2)
library(ggplot2)
library(tidyverse)
library(abind)
library(qgg)
library(Matrix)

#--------------------------------------------------------
# Data
#--------------------------------------------------------
# Sum of SNP weights
w <- foreach(SNP_class=SNP_classes, .combine=`+`) %do% {
  
  readRDS(paste0(input_folder, "weight_list-", SNP_class, ".rds"))
  
}

# Population structure
popInfo <- readRDS(paste0(input_folder, "Q.rds"))

X <- cbind("(Intercept)"=1, as.matrix(popInfo[, Q_variables]))

# GRM
if (fit_G) {
  
  G0 <- nearPD(readRDS(paste0(input_folder, "G.rds")))$mat %>%
    as.matrix()
  
} else {
  
  G0 <- NULL
  
}

# Phenotype data
Y_list <- list()

Y_list[["CP"]] <- foreach(VS_folder=VS_folders, .combine=rbind) %do% {
  
  readRDS(paste0(VS_folder, "pheno.rds"))[, traits] %>%
    as.matrix()
  
}

Y_list[["LOFO"]] <- readRDS(paste0(NAM_folder, "pheno.rds"))[, traits] %>%
  as.matrix()

# Validation sets
VS_list <- list()

VS_list[["CP"]] <- sapply(VS_folders, function(VS_folder) {
  
  readRDS(paste0(VS_folder, "pheno.rds")) %>%
    rownames()
  
}, simplify=FALSE)

VS_list[["LOFO"]] <- split(rownames(Y_list[["LOFO"]]),
                           gsub("E.+", "", rownames(Y_list[["LOFO"]])))

#--------------------------------------------------------
# Genomic prediction
#--------------------------------------------------------
CV <- list()

# Permutations
for (k in 0:n_perm) {
  
  cat("Permutation:", k, "\n")
  
  output_suffix <- paste0("-", paste(SNP_classes, collapse="_"),
                          "-", k)
  
  # Input: GRM for prioritized SNPs
  input_suffix <- paste0(SNP_class, "-", k)
  GRM_list <- readRDS(paste0(input_folder, "GRM_list-", input_suffix, ".rds"))
  
  for (var_name in names(GRM_list)) {
    
    GRM_list[[var_name]] <- nearPD(GRM_list[[var_name]]/w[var_name])$mat %>%
      as.matrix()
    
  }
  
  # Burden variables
  for (burden_variable in names(quantiles)) {
    
    cat("----------------------------------------------------\nSNP weight:",
        burden_variable,
        "\n----------------------------------------------------\n")
    
    # Quantiles
    for (q_val in quantiles[[burden_variable]]) {
      
      cat("\tQuantile:", ifelse(is.null(q_val), 0, q_val), "\n")
      
      var_name <- paste(c(burden_variable, q_val), collapse=".q")
      
      G1 <- GRM_list[[var_name]]
      
      # Validation scheme
      for (validation in c("CP", "LOFO")) {
        
        cat("\t\tValidation:", validation, "\n")
        
        Y <- Y_list[[validation]]
        VS <- VS_list[[validation]]
        
        for (trait in colnames(Y)) {
          
          cat("\t\t\tTrait:", trait, "\n")
          
          # Input
          obs <- rownames(Y)[is.finite(Y[, trait])]
          
          y.obs <- Y[obs, trait]
          X.obs <- X[obs, ]
          G.obs <- list(G0[obs, obs], G1[obs, obs])
          
          VS.obs <- lapply(VS, function(cv_split) {
            na.omit(match(cv_split, obs))
          })
          
          # Model fit
          model_file <- paste0(
            model_dir,
            paste(c(trait, validation, burden_variable, q_val, k), collapse="-"),
            ".rds"
          )
          
          if (file.exists(model_file) & ! overwrite) {
            
            fit <- readRDS(model_file)
            
          } else {
            
            fit <- greml(y=y.obs,
                         X=X.obs,
                         GRM=G.obs,
                         validate=VS.obs,
                         ncores=n_cores,
                         maxit=1000,
                         tol=1e-10)
            
            saveRDS(fit, model_file)
            
          }
          
          # Output
          r <- sapply(VS, function(cv_split) {
            idx <- match(cv_split, names(fit$yobs))
            with(fit, cor(ypred[idx], yobs[idx], use="complete"))
          })
          
          out <- data.frame(
            Constraint=plot_legend[burden_variable],
            input=k,
            trait=plot_legend[trait],
            burden_variable=burden_variable,
            quantile=ifelse(is.null(q_val), 0, q_val),
            family=names(VS.obs),
            r=r,
            MSE=fit$accuracy$MSPE,
            row.names=NULL
          )
          
          CV[[validation]] <- rbind(CV[[validation]], out)
          
          saveRDS(CV, output_file)
          
        } # for: trait
        
      } # for: validation
      
    } # for: quantile
    
  } # for: burden_variable
  
} # for: k
