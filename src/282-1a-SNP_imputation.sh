#!/usr/bin/env bash

home_dir=~/QGG_plants/snp_constraint_prediction/
input_dir=${home_dir}data/

in_prefix=${input_dir}Hmp321/AGPv3/merged_flt_c
Hmp_prefix=${input_dir}Hmp321/AGPv3/hmp321_without282set_agpv3_chr
ref_prefix=${input_dir}Hmp321/AGPv3/hmp321_282_agpv3_merged_chr

G282_prefix=${input_dir}282set/AGPv3/c

beagle_version=beagle.28Sep18.793.jar

n_chromosomes=10

checkpoint=4

if [ ! -d ${input_dir}tmp ]; then mkdir ${input_dir}tmp; fi
cd ${input_dir}tmp

###################################################################
# 0. Indexing input
###################################################################
if [ ${checkpoint} -le 0 ]
then

echo Indexing VCF files...

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

gunzip -f ${in_prefix}${i}.vcf.gz &

sleep 1

gunzip -f ${G282_prefix}${i}_282_corrected_onHmp321.vcf.gz &

done
wait

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

bgzip -f ${in_prefix}${i}.vcf &

sleep 1

bgzip -f ${G282_prefix}${i}_282_corrected_onHmp321.vcf &

done
wait

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

tabix -f -p vcf ${in_prefix}${i}.vcf.gz &

sleep 1

tabix -f -p vcf ${G282_prefix}${i}_282_corrected_onHmp321.vcf.gz &

done
wait

fi


###################################################################
# 1. Format data at Hapmap321
###################################################################
if [ ${checkpoint} -le 1 ]
then

# Discarding 282 samples
echo Discarding 282 samples...

bcftools query -l ${in_prefix}1.vcf.gz | grep -v 282set > to_keep.txt

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

bcftools annotate -x ^FORMAT/GT -Ov -o ${in_prefix}${i}.vcf ${in_prefix}${i}.vcf.gz &

done
wait

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

sed -i '/^#/! s/0\/1/\.\/\./g; s/1\/0/\.\/\./g' ${in_prefix}${i}.vcf &

done
wait

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

vcftools --vcf ${in_prefix}${i}.vcf \
--out ${Hmp_prefix}${i} \
--keep to_keep.txt \
--remove-indels \
--max-missing 0.5 \
--min-alleles 2 \
--max-alleles 2 \
--mac 3 \
--recode &

done
wait

# Indexing
echo Indexing...

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

bgzip -f ${Hmp_prefix}${i}.recode.vcf &

done
wait

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

tabix -f -p vcf ${Hmp_prefix}${i}.recode.vcf.gz &

done
wait

fi


###################################################################
# 2. Format data at 282 and Ames
###################################################################
if [ ${checkpoint} -le 2 ]
then

# Filtering markers
echo Filtering markers in 282 and Ames...

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

bcftools query -f '%CHROM\t%POS\n' ${Hmp_prefix}${i}.recode.vcf.gz > hmp321_agpv3_chr${i}.recode.pos &

done
wait

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

bcftools annotate -x ^FORMAT/GT -Ov -o ${G282_prefix}${i}_282_corrected_onHmp321.vcf ${G282_prefix}${i}_282_corrected_onHmp321.vcf.gz &

done
wait

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

sed -i '/^#/! s/0\/1/\.\/\./g; s/1\/0/\.\/\./g' ${G282_prefix}${i}_282_corrected_onHmp321.vcf &

done
wait

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

vcftools --vcf ${G282_prefix}${i}_282_corrected_onHmp321.vcf \
--out ${G282_prefix}${i} \
--positions hmp321_agpv3_chr${i}.recode.pos \
--recode &

done
wait

# Indexing
echo Indexing...

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

bgzip -f ${G282_prefix}${i}.recode.vcf &

done
wait

for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

tabix -f -p vcf ${G282_prefix}${i}.recode.vcf.gz &

done
wait

fi


###################################################################
# 3. Reference panel: 282 samples + Hapmap321
###################################################################
if [ ${checkpoint} -le 3 ]
then

echo Merging 282 and Hapmap321...

# Merging
for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

bcftools merge -m all -Oz -o ${ref_prefix}${i}-unfiltered.vcf.gz --threads 10 \
${Hmp_prefix}${i}.recode.vcf.gz ${G282_prefix}${i}.recode.vcf.gz &

done
wait

# Normalizing
for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

bcftools norm -m +any -Oz -o ${ref_prefix}${i}-normalized.vcf.gz ${ref_prefix}${i}-unfiltered.vcf.gz &

done
wait

for i in $(seq 1 1 ${n_chromosomes}); do rm ${ref_prefix}${i}-unfiltered.vcf.gz; done

# Filtering
for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

bcftools view -m2 -M2 -v snps -Oz -o ${ref_prefix}${i}-unsorted.vcf.gz ${ref_prefix}${i}-normalized.vcf.gz &

done
wait

for i in $(seq 1 1 ${n_chromosomes}); do rm ${ref_prefix}${i}-normalized.vcf.gz; done

# Sorting
for i in $(seq 1 1 ${n_chromosomes})
do

sleep 1

bcftools sort -Oz -o ${ref_prefix}${i}.vcf.gz ${ref_prefix}${i}-unsorted.vcf.gz &

done
wait

for i in $(seq 1 1 ${n_chromosomes}); do rm ${ref_prefix}${i}-unsorted.vcf.gz; done

fi


###################################################################
# 4. Imputation in Hapmap3.2.1
###################################################################
cd ${home_dir}

if [ ${checkpoint} -le 4 ]
then

echo Imputing in Hapmap321...

for i in $(seq 1 1 ${n_chromosomes})
do

echo Chromosome ${i}

sample=${ref_prefix}${i}
ref=${ref_prefix}${i}.imputed

java -Xmx400g -jar ${beagle_version} gt=${sample}.vcf.gz burnin=10 iterations=15 ne=1000 out=${ref}

done

fi