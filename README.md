# README #

SNP constraint prediction 

### Project ###
* __Tentative title__: Calibration of genomic annotations by evolutionary constraint improves prioritization of causal variants in maize
* __Summary__: We predicted evolutionary constraint by genomic annotations to avoid the caveats of measured evolutionary constraint (lack of quantitative resolution, missing values) and prioritize SNP fitness effects in maize

### Content ###

* _src/_: Bash and R code on data processing and analysis
* _figs/_: Manuscript figures

### Contacts ###

* Owner: Guillaume Ramstein (gr226@cornell.edu, ramstein@qgg.au.dk)
* Other contacts: Ed Buckler, Sara Miller, Cinta Romay (Buckler lab)

### MIT License ###

Copyright (c) 2022 Guillaume P. Ramstein, Edward S. Buckler

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

